# Release steps

After the state of `develop` is what we want and tested, etc.

1. comic-writer-web repo: Make sure `version` in `package.json` has been updated appropriately.
2. www repo: Update the Writer's Guide if necessary.
3. www repo: Write a release notes blog post. Mimic the format of past release notes posts.
4. www repo: Publish the latest version of the website by git pushing to `main`. This needs to be live *before* the app changes go out because the app links to the release notes.
5. comic-writer-web repo: Merge `develop` to `main` and git push.
6. netlify: futz around in netlify's UI and figure out how to publish the build from step 5. **The new version of the app is live after this succeeds.**
7. twitter: Make a tweet to announce the new release. In the tweet make sure to:
  - Link to the app and new version's release notes
  - Also use some hashtags `#comics #writing` to boost discoverability
