# Performance testing

We have some perf tests that measure timing when a writer types into the editor. Typing in the editor is the most (and only?) performance sensitive part of the app.

## Test Cases

### 1. Toggle page at top of script

Insert a page heading at the top of the script which forces all existing pages to be reparsed. It's the most expensive thing a writer can do in terms of processing but isn't very realistic.

### 2. Typing at top of script

Types text in the metdata area. This doesn't require any pages to be reparsed and is very cheap.

### 3. Typing in caption

Typing in a caption.  It's good for timing word count code. This is a decent simulation of actual user behavior.

### 4. Typing in panel description

Typing in a panel description. This is a decent simulation of actual user behavior.

## Running a single perf test cycle

1. Launch the dev server (`yarn start`)
2. Run perf tests (`yarn run test:perf`)
3. Open `test/perf/reports/<pick a test case>/index.html` to view results

The contents of `test/perf/reports` are gitignore'd and should not be saved in git.

## Perf testing workflow

1. Close any extra apps so your system load is relatively stable.
2. Before your changes: Run a perf test cycle to get baseline numbers.
3. Make a code change that you want to measure.
4. Run a perf test cycle and view results.
5. Repeat steps as necesary.

## Value of results

Since everyone's machine is different, results of a perf test are only relevant when compared to other results generated on your machine.

System load also matters so maybe results from 5 months ago might not be a fair comparison to results from today.
