/*
Sanity check to make sure dev server is up.
*/

const needle = require('needle');
const url = 'http://localhost:3000';

(async () => {
  try {
    await needle('get', url);
    good();
  } catch (e) {
    bad();
  }
})();

function bad() {
  console.error(`No server responding at ${url} -- is the dev server running? (yarn start)`);
  process.exit(1);
}

function good() {
  process.exit(0);
}