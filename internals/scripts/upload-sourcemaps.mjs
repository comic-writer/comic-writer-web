import fetch from "node-fetch";
import { fileFromSync } from "fetch-blob/from.js";
import { FormData } from "formdata-polyfill/esm.min.js";
import { readdir } from "fs/promises";
import * as path from "path";

/**
 * Upload sourcemaps to honeybadger.
 *
 * This should be run right after a build that is being deployed to production.
 *
 * Based on https://github.com/honeybadger-io/honeybadger-webpack
 */

if (isProductionBuild()) {
  try {
    await uploadSourcemaps(process.cwd());
  } catch (e) {
    console.error(e);
  }
} else {
  console.log("Not a production build, skipping");
}

async function uploadSourcemaps(baseDir) {
  const jsDir = path.join(baseDir, "build/static/js");

  const filenames = await readdir(jsDir, {
    encoding: "utf8",
  });

  const mapFilePaths = filenames
    .filter((filename) => filename.endsWith(".map"))
    .map((filename) => path.join(jsDir, filename));

  for (const mapFilePath of mapFilePaths) {
    const sourceFilePath = toSourceFilePath(mapFilePath);

    await uploadSourcemap({
      sourceFileUrl: deployedAssetUrl(path.basename(sourceFilePath)),
      sourceFilename: path.basename(sourceFilePath),
      sourceFile: fileFromSync(sourceFilePath),
      mapFilename: path.basename(mapFilePath),
      mapFile: fileFromSync(mapFilePath),
    });
  }
}

async function uploadSourcemap(params) {
  const form = new FormData();
  form.append("api_key", apiKey());
  form.append("minified_url", params.sourceFileUrl);
  form.append("minified_file", params.sourceFile);
  form.append("source_map", params.mapFile);
  form.append("revision", revision());

  const resp = await fetch("https://api.honeybadger.io/v1/source_maps", {
    method: "POST",
    body: form,
    redirect: "follow",
    opts: {
      retries: 3,
      maxTimeout: 1000,
    },
  });

  if (!resp.ok) {
    // Attempt to parse error details from response
    let details;
    try {
      const body = await resp.json();

      if (body && body.error) {
        details = body.error;
      } else {
        details = `${resp.status} - ${resp.statusText}`;
      }
    } catch (parseErr) {
      details = `${resp.status} - ${resp.statusText}`;
    }

    throw new Error(
      `Could not upload ${params.sourceFilename} to Honeybadger: ${details}`
    );
  }

  console.log(`Uploaded ${params.sourceFilename} to Honeybadger`);
}

function toSourceFilePath(mapFilePath) {
  return mapFilePath.slice(0, -".map".length);
}

function deployedAssetUrl(filename) {
  return `https://comicwriter.io/static/js/${filename}`;
}

function apiKey() {
  return process.env.REACT_APP_HONEYBADGER_KEY;
}

function revision() {
  return process.env.REACT_APP_REVISION;
}

function isProductionBuild() {
  // Netlify defines these env vars at build time
  // https://docs.netlify.com/configure-builds/environment-variables/#read-only-variables
  return process.env.NETLIFY === "true" && process.env.CONTEXT === "production";
}
