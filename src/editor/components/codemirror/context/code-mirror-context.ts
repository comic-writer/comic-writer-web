import { Editor } from 'codemirror';
import { createContext } from 'react';

export type CodeMirrorApi = {
  editor: Editor,
}

export const CodeMirrorContext = createContext<CodeMirrorApi | null>(null);
