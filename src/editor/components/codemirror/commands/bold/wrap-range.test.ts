import { wrapRange } from './wrap-range';

describe('wrapRange', () => {
  it('everything selected', () => {
    const result = wrapRange('house', 0, 5, '[', ']');

    expect(result).toBe('[house]');
  });

  it('front selected', () => {
    const result = wrapRange('house', 0, 2, '[', ']');

    expect(result).toBe('[ho]use');
  });

  it('middle selected', () => {
    const result = wrapRange('house', 1, 4, '[', ']');

    expect(result).toBe('h[ous]e');
  });

  it('end selected', () => {
    const result = wrapRange('house', 3, 5, '[', ']');

    expect(result).toBe('hou[se]');
  });

  it('blank line', () => {
    const result = wrapRange('', 0, 0, '[', ']');

    expect(result).toBe('[]');
  });

  it('nothing selected', () => {
    const result = wrapRange('house', 2, 2, '[', ']');

    expect(result).toBe('ho[]use');
  });
});
