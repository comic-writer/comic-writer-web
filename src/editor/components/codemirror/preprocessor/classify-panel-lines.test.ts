import classifyLines from './classify';
import { LineClassification } from './types';

type TestCase = [string, number, LineClassification];

describe('classify panel lines', () => {
  describe('single line edit', () => {
    let classifier: (line: string, lineNumber: number) => LineClassification;

    beforeEach(() => {
      classifier = classifyLines(3, 1, 0);
    });

    const testCases: Array<TestCase> = [
      // cursor still on this line
      ['panel', 3, { type: 'panel', frozen: true }],
      ['panel 4', 3, { type: 'panel', frozen: true }],
      ['panel 4 blah', 3, { type: 'regular', frozen: false }],
      // cursor not on this line
      ['panel', 2, { type: 'panel', frozen: false }],
      ['panel 5', 2, { type: 'panel', frozen: false }],
      ['panel 5 blah', 2, { type: 'regular', frozen: false }],
    ];

    testCases
      .forEach(([line, lineNumber, expected]) => {
        test(`line: "${line}"`, () => {
          const result = classifier(line, lineNumber);

          expect(result).toEqual(expected);
        });
      });
  });

  describe('multi line edit', () => {
    let classifier: (line: string, lineNumber: number) => LineClassification;

    beforeEach(() => {
      classifier = classifyLines(3, 2, 0);
    });

    const testCases: Array<TestCase> = [
      // cursor still on this line
      ['panel', 3, { type: 'panel', frozen: false }],
      ['panel 4', 3, { type: 'panel', frozen: false }],
      ['panel 4 blah', 3, { type: 'regular', frozen: false }],
      // cursor not on this line
      ['panel', 2, { type: 'panel', frozen: false }],
      ['panel 5', 2, { type: 'panel', frozen: false }],
      ['panel 5 blah', 2, { type: 'regular', frozen: false }],
    ];

    testCases
      .forEach(([line, lineNumber, expected]) => {
        test(`line: "${line}", line number: ${lineNumber}`, () => {
          const result = classifier(line, lineNumber);

          expect(result).toEqual(expected);
        });
      });
  });
});
