// events from Editor

import { FileSystemHandle } from '../file-system';

export interface EditorChangeEvent {
  /** New contents of the editor */
  lines: Array<string>;
}

export interface ScrollPosition {
  /**
   * Zero-based line number of the line that the editor is on. This is not
   * necessarily the top visible line.
   */
  currentLine: number;
}

export type EditorScrollEvent = ScrollPosition;

// Prop types and events for Outline component

interface BaseOutlineItem {
  id: string;
  type: string;
  lineNumber: number;
  current: boolean;
}

export interface SpreadOutlineItem extends BaseOutlineItem {
  type: 'spread';
  label: string;
}

export interface PanelOutlineItem extends BaseOutlineItem {
  type: 'panel';
  panelNumber: number;
  description: string;
}

export type OutlineItem = SpreadOutlineItem | PanelOutlineItem;

export interface OutlineItemSelectionEvent {
  /** Zero-based line number of the selected item */
  lineNumber: number;
}

/**
 * Fired by outline items when they need to be centered in the outline.
 */
export interface CenteringRequestEvent {
  /** The element to be centered */
  element: HTMLElement;
}

/**
 * Commands that the editor can perform.
 */
export interface EditorCommand {
  name: EditorCommandName;
}

export type EditorCommandName =
  | 'insertPage'
  | 'insertPanel'
  | 'dialogue'
  | 'caption'
  | 'sfx'
  | 'bold'
;

export interface ScriptReplacement {
  handle: FileSystemHandle | null;
  source: string;
}
