/** Dummy value for when the editor doesn't have a target line yet */
export const NO_TARGET_LINE = -1;
