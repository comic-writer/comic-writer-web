import React from 'react';
import { MessageBox } from './MessageBox';
import '../../../index.css'

export default {
  component: MessageBox,
  title: 'Modals/MessageBox',
  args: {
    title: 'Sem Vestibulum Inceptos',
    message: 'Sed posuere consectetur est at lobortis.',
    /**
     * Button labels - onResult() is invoked with the index of the selected button
     */
    buttons: ['Alpha', 'Beta'],
    /** Button index to put initial focus on */
    defaultIndex: 1,
    /** When dialog is closed without selecting a button, this index is returned */
    cancelIndex: 0,
    onResult: i => alert(`Button at index ${i} was clicked.`),
  }
};

export const Default = args => <MessageBox{...args} />;

export const WithUnsafe = args => (
  <MessageBox
    {...args}
    unsafeButtons={['Unsafe']}
    defaultIndex={2}
    cancelIndex={1}
  />
);
