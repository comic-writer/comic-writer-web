import React from 'react';
import { OnBoarding } from './OnBoarding';
import '../../../index.css'

export default {
  component: OnBoarding,
  title: 'Modals/OnBoarding',
  args: {
    onClose: i => alert(`onClose event`),
  }
};

export const Default = args => <OnBoarding{...args} />;
