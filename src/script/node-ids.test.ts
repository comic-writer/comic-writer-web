import { NodeIds } from './node-ids';
import * as parts from '../comic-part-types';

describe('NodeIds', () => {
  describe('without prefix', () => {
    test('repeating type increments count', () => {
      const nodeIds = new NodeIds();

      expect(nodeIds.next(parts.PANEL)).toBe('panel-0');
      expect(nodeIds.next(parts.PANEL)).toBe('panel-1');
      expect(nodeIds.next(parts.PANEL)).toBe('panel-2');
    });

    test('each type gets its own count', () => {
      const nodeIds = new NodeIds();

      expect(nodeIds.next(parts.PANEL)).toBe('panel-0');
      expect(nodeIds.next(parts.CAPTION)).toBe('caption-0');
      expect(nodeIds.next(parts.SFX)).toBe('sfx-0');

      expect(nodeIds.next(parts.SFX)).toBe('sfx-1');
      expect(nodeIds.next(parts.SFX)).toBe('sfx-2');
      expect(nodeIds.next(parts.SFX)).toBe('sfx-3');

      expect(nodeIds.next(parts.PANEL)).toBe('panel-1');
      expect(nodeIds.next(parts.CAPTION)).toBe('caption-1');
    });
  });

  describe('with prefix', () => {
    test('repeating type increments count', () => {
      const nodeIds = new NodeIds('prefix-');

      expect(nodeIds.next(parts.PANEL)).toBe('prefix-panel-0');
      expect(nodeIds.next(parts.PANEL)).toBe('prefix-panel-1');
      expect(nodeIds.next(parts.PANEL)).toBe('prefix-panel-2');
    });

    test('each type gets its own count', () => {
      const nodeIds = new NodeIds('prefix-');

      expect(nodeIds.next(parts.PANEL)).toBe('prefix-panel-0');
      expect(nodeIds.next(parts.CAPTION)).toBe('prefix-caption-0');
      expect(nodeIds.next(parts.SFX)).toBe('prefix-sfx-0');

      expect(nodeIds.next(parts.SFX)).toBe('prefix-sfx-1');
      expect(nodeIds.next(parts.SFX)).toBe('prefix-sfx-2');
      expect(nodeIds.next(parts.SFX)).toBe('prefix-sfx-3');

      expect(nodeIds.next(parts.PANEL)).toBe('prefix-panel-1');
      expect(nodeIds.next(parts.CAPTION)).toBe('prefix-caption-1');
    });
  });
});
