import { createMemoizedMapper } from './memoized-mapper';

describe('memoized mapper', () => {
  test('maps an array', () => {
    const mapper = createMemoizedMapper<number, number>(i => i + 1);

    const result = mapper([1, 2, 3]);

    expect(result).toEqual([2, 3, 4]);
  });

  test('maps an array, multiple calls', () => {
    const mapper = createMemoizedMapper<number, number>(i => i + 1);

    const result = mapper([1, 2, 3]);

    expect(result).toEqual([2, 3, 4]);

    const result2 = mapper([10, 20, 30]);

    expect(result2).toEqual([11, 21, 31]);
  });

  test('only invokes `map` for input elements different from previous input element', () => {
    const map = jest.fn((i: number) => i + 1);
    const mapper = createMemoizedMapper<number, number>(map);

    mapper([1, 2, 3]); // 3 calls
    mapper([1, 20, 3]); // 1 call
    mapper([1, 2, 3]); // 1 call

    expect(map).toHaveBeenCalledTimes(5);

    // nothing was memoized yet so had to call map for every input
    expect(map).toHaveBeenNthCalledWith(1, 1);
    expect(map).toHaveBeenNthCalledWith(2, 2);
    expect(map).toHaveBeenNthCalledWith(3, 3);

    // only called map for the new input
    expect(map).toHaveBeenNthCalledWith(4, 20);

    // 2 was not in the previous input so had to map it
    expect(map).toHaveBeenNthCalledWith(5, 2);
  });
});
