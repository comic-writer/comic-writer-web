import {
  Spread,
  PreSpread,
} from '../parser/nodes';

export interface PanelCount {
  count: number;
  /** Zero-based line number */
  lineNumber: number;
}

export interface WordCount {
  count: number;
  /** Zero-based line number */
  lineNumber: number;
  isSpread: boolean;
}

export interface FullScript {
  preSpread: Array<PreSpread>;
  spreads: Array<Spread>;
}
