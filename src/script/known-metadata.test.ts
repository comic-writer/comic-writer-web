import { BlankLine, Metadata, Paragraph, PreSpread } from "../parser/nodes";
import {
  KnownMetadata,
  headerText,
  isComicTitleMetadata,
} from "./known-metadata";

describe("KnownMetadata", () => {
  test('still has title metadata when empty', () => {
    const known = new KnownMetadata([]);

    expect(known.hasTitle).toBeTruthy();
    expect(known.title).toBe("Untitled");
  });

  test("input key case does not matter", () => {
    const known = new KnownMetadata([
      ["TITLE", "My Comic"],
      ["issue", "3"],
      ["By", "John Doe"],
    ]);

    expect(known.hasTitle).toBeTruthy();
    expect(known.title).toBe("My Comic");

    expect(known.hasIssue).toBeTruthy();
    expect(known.issue).toBe("3");

    expect(known.hasBy).toBeTruthy();
    expect(known.by).toBe("John Doe");
  });

  test("uses latest value when there are duplicate keys", () => {
    const known = new KnownMetadata([
      ["By", "John Doe 1"],
      ["By", "John Doe 2"],
      ["by", "John Doe 3"],
    ]);

    expect(known.hasBy).toBeTruthy();
    expect(known.by).toBe("John Doe 3");
  });

  test("empty", () => {
    const known = new KnownMetadata([]);

    expect(known.hasTitle).toBeTruthy();
    expect(known.hasIssue).toBeFalsy();
    expect(known.hasBy).toBeFalsy();
  });

  test("no known metadata", () => {
    const known = new KnownMetadata([["Date", "2021-03-01"]]);

    expect(known.hasTitle).toBeTruthy();
    expect(known.hasIssue).toBeFalsy();
    expect(known.hasBy).toBeFalsy();
  });

  test("creating from PreSpread nodes", () => {
    const preSpread: Array<PreSpread> = [
      new Metadata("title", "My Comic"),
      new BlankLine(),
      new Metadata("issue", "6"),
      new Metadata("by", "John Doe"),
      new Paragraph(),
    ];

    const known = KnownMetadata.create(preSpread);

    expect(known.hasTitle).toBeTruthy();
    expect(known.title).toBe("My Comic");

    expect(known.hasIssue).toBeTruthy();
    expect(known.issue).toBe("6");

    expect(known.hasBy).toBeTruthy();
    expect(known.by).toBe("John Doe");
  });
});

describe("headerText", () => {
  test("headerText with all combos", () => {
    expect(headerText("My Comic", "3", "John Doe")).toBe(
      "My Comic #3 / John Doe"
    );
    expect(headerText("My Comic", "3", undefined)).toBe("My Comic #3");
    expect(headerText("My Comic", undefined, "John Doe")).toBe(
      "My Comic / John Doe"
    );
    expect(headerText(undefined, "3", "John Doe")).toBe("John Doe");
    expect(headerText("My Comic", undefined, undefined)).toBe("My Comic");
    expect(headerText(undefined, "3", undefined)).toBe(null);
    expect(headerText(undefined, undefined, "John Doe")).toBe("John Doe");
    expect(headerText(undefined, undefined, undefined)).toBe(null);
  });
});

describe("isComicTitleMetadata", () => {
  test("accepts comic title metadata", () => {
    expect(
      isComicTitleMetadata(new Metadata("title", "My Comic"))
    ).toBeTruthy();
    expect(isComicTitleMetadata(new Metadata("issue", "3"))).toBeTruthy();
    expect(isComicTitleMetadata(new Metadata("By", "John Doe"))).toBeTruthy();
  });

  test("rejects other metadata", () => {
    expect(isComicTitleMetadata(new Metadata("year", "2021"))).toBeFalsy();
  });

  test("rejects non metadata nodes", () => {
    expect(isComicTitleMetadata(new BlankLine())).toBeFalsy();
    expect(isComicTitleMetadata(new Paragraph())).toBeFalsy();
  });
});
