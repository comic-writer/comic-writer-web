import * as parts from '../comic-part-types';
import { Spread } from '../parser/nodes';

export function* spreadsAndChildren(spreads: Array<Spread>) {
  for (const spread of spreads) {
    yield spread;

    for (const child of spread.children) {
      yield child;

      if (child.type === parts.PANEL) {
        yield* child.children;
      }
    }
  }
}
