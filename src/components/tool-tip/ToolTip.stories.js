import React from 'react';
import ToolTip from './ToolTip';
import '../../index.css'

export default {
  component: ToolTip,
  title: 'Universal/ToolTip',
  args: {
    onMouseEnter: e => console.log(e),
    onMouseLeave: e => console.log(e),
    children: [
      'ToolTip Content',
      <div style={{border: '1px solid grey', padding: '1em'}}>Hover Me!</div>
    ]
  }
};

export const Default = args => <ToolTip{...args} />;
