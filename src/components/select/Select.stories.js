import { Select } from './Select';
import '../../index.css'

export default {
  component: Select,
  title: 'Universal/Select',
};

const Template = args => <Select {...args} />;

export const Default = Template.bind({});
Default.args = {
  id: 'bold-style',
  label: 'Bold lettering export style',
  name: 'bold-style',
  options: toOptions([
    ['Bold', 'bold'],
    ['Underline', 'underline'],
    ['Bold & Underline', 'bold-and-underline'],
  ]),
  onChange(option) {
    console.log(option);
  }
};

export const Placeholder = Template.bind({});
Placeholder.args = {
  ...Default.args,
  placeholder: 'Select a style'
};

export const InitialValue = Template.bind({});
InitialValue.args = {
  ...Default.args,
  initialValue: 'underline'
};

function toOptions(opts) {
  return opts.map(opt => ({
    label: opt[0],
    value: opt[1]
  }));
}
