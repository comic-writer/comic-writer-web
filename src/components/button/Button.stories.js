import React from 'react';
import Button from './Button';
import '../../index.css'

export default {
  component: Button,
  title: 'Universal/Button',
  args: {
    onClick: () => alert("Button Clicked"),
    children: "Button",
    id: '1',
    primary: false
  }
};

export const Default = args => <Button {...args} />;


export const Primary = Default.bind({});
Primary.args = {
  primary: true
};
