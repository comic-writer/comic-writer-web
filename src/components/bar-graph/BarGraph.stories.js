import React from 'react';
import { BarGraph } from './BarGraph';
import '../../index.css'

export default {
  component: BarGraph,
  title: 'Universal/BarGraph',
};

const Template = args => <BarGraph {...args} />;

export const Default = Template.bind({});
Default.args = {
  height: "350px",
  stats: Array(21)
    .fill(null)
    .map((_, index) => ({
      value: index * 5,
      index,
      lineNumber: index * 10,
      pages: [index + 1]
    })),
  onBarSelection: (lineNumber) => console.log(`selected bar, lineNumber: ${lineNumber}`),
  onBarHover: (index) => console.log(`hovered bar, index: ${index}`),
  xAxisLabel: 'Page',
  caption: 'Stats per page',
};

export const VerticalLine = Template.bind({});
VerticalLine.args = {
  ...Default.args,
  chartPosition: 30,
};

export const HighlightedBar = Template.bind({});
HighlightedBar.args = {
  ...Default.args,
  highlightedIndex: 4,
};

export const MultiPageSpreads = Template.bind({});
MultiPageSpreads.args = {
  ...Default.args,
  stats: Default.args.stats.map((stat, index) => ({
    ...stat,
    pages: index % 7 === 0
      ? [1, 2]
      : index % 9 === 0
      ? [11, 12]
      : [index + 1]

  }))
};

export const SingleDigitFloatingPointValues = Template.bind({});
SingleDigitFloatingPointValues.args = {
  ...Default.args,
  decimalDigits: 1,
};
