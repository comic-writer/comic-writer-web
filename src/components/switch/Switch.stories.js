import React from 'react';
import Switch from './Switch';
import '../../index.css'

export default {
  component: Switch,
  title: 'Universal/Switch',
  args: {
    on: true,
    onChange: on => console.log(`Changed to ${on}`),
    label: 'This is a label',
    id: 'id1',
    onString: 'On',
    offString: 'Off'
  }
};

export const Default = args => <Switch {...args} />;


export const Block = Default.bind({});
Block.args = {
  block: true
};
