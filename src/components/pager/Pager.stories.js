import React from 'react';
import Pager from './Pager';
import '../../index.css'

export default {
  component: Pager,
  title: 'Universal/Pager',
  args: {
    showChildAtIndex: 0
  }
};

export const Default = args =>
  <Pager {...args}>
   <div>Index 0</div>
   <div>Index 1</div>
   <div>Index 2</div>
   <div>Index 3</div>
  </Pager>;
