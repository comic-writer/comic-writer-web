import React from 'react';
import { Hotkey } from './Hotkey';
import '../../index.css';

export default {
  component: Hotkey,
  title: 'Hotkey',
  args: {
    keyCombo: ['Cmd', 'A'],
    animated: false
  }
};

export const Default = args => <Hotkey {...args} />;

export const Animated = Default.bind({});
Animated.args = {
  animated: true
};
