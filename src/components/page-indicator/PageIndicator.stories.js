import React from 'react';
import PageIndicator from './PageIndicator';
import '../../index.css'

export default {
  component: PageIndicator,
  title: 'Universal/PageIndicator',
  args: {
    count: 5,
    currentIndex: 0,
    onClick: i => alert(`Button ${i} Clicked`),
  }
};

export const Default = args => <PageIndicator {...args} />;
