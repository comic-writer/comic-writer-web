import React from 'react';
import { Menu } from './Menu';
import '../../index.css'

export default {
  component: Menu,
  title: 'Universal/Menu',
  args: {
    menuData : [
      {
        menuName: 'Menu 0',
        menuChildren: [
          {
            menuName: 'Menu Item 1',
            type: 'item',
            onClick: () => alert('Menu Item 0')
          },
          { type: 'divider' },
          {
            menuName: 'Menu Item 1',
            type: 'item',
            onClick: () => alert('Menu Item 1'),
            hotkey: 'Meta-O'
          },
          {
            menuName: 'Menu Item 2',
            type: 'item',
            onClick: () => alert('Menu Item 2'),
            hotkey: 'Meta-S'
          }
        ]
      },
      {
        menuName: 'Menu 1',
        menuChildren: [
          {
            menuName: `Menu Item 0`,
            type: 'item',
            onClick: () => alert('Menu Item 0'),
            hotkey: 'Meta-/'
          },
          { type: 'divider' },
          {
            menuName: 'Menu Item 1',
            type: 'item',
            onClick: () => alert('Menu Item 1'),
          }
        ]
      }
    ],
  }
};

export const Default = args => <Menu{...args} />;
