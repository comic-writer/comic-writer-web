import React from 'react';
import { Link } from './Link';
import '../../index.css'

export default {
  component: Link,
  title: 'Universal/Link',
  args: {
    url: '/guide'
  }
};

export const Internal = args => (
  <Link {...args} url="/guide">
    Writer's Guide
  </Link>
);

export const External = args => (
  <Link {...args} url="https://example.com">
    Example External
  </Link>
);

export const NewTab = args => (
  <Link {...args} newTab>
    Writer's Guide
  </Link>
);
