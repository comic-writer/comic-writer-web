import React from 'react';
import ToolBar from './ToolBar';
import Button from '../button/Button';
import '../../index.css'

export default {
  component: ToolBar,
  title: 'Universal/ToolBar',
  args: {}
};

export const Default = args => (
  <ToolBar>
    <React.Fragment />
    <Button>Cancel</Button>
    <Button primary>Primary</Button>
  </ToolBar>
);

export const Split = args => (
  <ToolBar>
    <Button>Help</Button>
    <React.Fragment />
    <Button>Cancel</Button>
    <Button primary>Primary</Button>
  </ToolBar>
);

export const Centered = args => (
  <ToolBar>
    <React.Fragment />
    <Button>Centered</Button>
    <React.Fragment />
  </ToolBar>
);

export const NonButton = args => (
  <ToolBar>
    <div>Left text</div>
    <React.Fragment />
    <div>Right text</div>
  </ToolBar>
);