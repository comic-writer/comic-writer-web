import countWords, { TextChunk } from './count-words';

describe('countWords', () => {
  test('empty string', () => {
    const result = countWords(chunks(''));

    expect(result).toEqual(0);
  });

  test('single word', () => {
    const result = countWords(chunks('dog'));

    expect(result).toEqual(1);
  });

  test('multi word', () => {
    const result = countWords(chunks('dog cat house'));

    expect(result).toEqual(3);
  });

  test('single word in multi chunks', () => {
    const result = countWords(chunks('d', 'o', 'g'));

    expect(result).toEqual(1);
  });

  test('multi word in multi chunks', () => {
    const result = countWords(chunks('dog', ' or ', 'cat'));

    expect(result).toEqual(3);
  });

  test('no chunks', () => {
    const result = countWords(chunks());

    expect(result).toEqual(0);
  });

  test('single letters count as words', () => {
    const result = countWords(chunks('a b c'));

    expect(result).toEqual(3);
  });

  test('words with puncuation between are separate words', () => {
    const result = countWords(chunks('It was good. But it'));

    expect(result).toEqual(5);
  });

  test('numbers count as words', () => {
    const result = countWords(chunks('dog 900 cat'));

    expect(result).toEqual(3);
  });

  test('hyphenated word is 2 words', () => {
    const result = countWords(chunks('spider-man'));

    expect(result).toEqual(2);
  });

  test('word with apostrophe is one word', () => {
    const result = countWords(chunks('can\'t'));

    expect(result).toEqual(1);
  });

  test('word with multi apostrophes is still one word', () => {
    const result = countWords(chunks('c\'an\'t'));

    expect(result).toEqual(1);
  });
});

function chunks(...strings: Array<string>): Array<TextChunk> {
  return strings.map(str => ({
    content: str
  }));
}
