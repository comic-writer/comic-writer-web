import * as regexes from './regexes';

describe('regexes', () => {
  describe('metadata', () => {
    test('happy path', () => {
      const result = regexes.METADATA_REGEX.exec('name: value');

      expect(result).toBeTruthy();
      expect(result!.length).toBe(3);
      expect(result![1]).toBe('name');
      expect(result![2]).toBe('value');
    });

    test('no space after colon', () => {
      const result = regexes.METADATA_REGEX.exec('name:value');

      expect(result).toBeTruthy();
      expect(result!.length).toBe(3);
      expect(result![1]).toBe('name');
      expect(result![2]).toBe('value');
    });

    test('case insensitive', () => {
      const result = regexes.METADATA_REGEX.exec('Name: VALUE');

      expect(result).toBeTruthy();
      expect(result!.length).toBe(3);
      expect(result![1]).toBe('Name');
      expect(result![2]).toBe('VALUE');
    });

    test('no value', () => {
      const result = regexes.METADATA_REGEX.exec('name:');

      expect(result).toBeFalsy();
    });

    test('http url', () => {
      const result = regexes.METADATA_REGEX.exec('http://example.com');

      expect(result).toBeFalsy();
    });

    test('https url', () => {
      const result = regexes.METADATA_REGEX.exec('https://example.com');

      expect(result).toBeFalsy();
    });

    test('only ignores http/https urls', () => {
      const result = regexes.METADATA_REGEX.exec('ftp://example.com');

      expect(result).toBeTruthy();
      expect(result!.length).toBe(3);
      expect(result![1]).toBe('ftp');
      expect(result![2]).toBe('//example.com');
    });
  });
});
