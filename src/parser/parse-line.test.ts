import { Sequence } from '../util/sequence';
import {
  parseSpread,
  parsePanel,
  parseMetadata,
  parseParagraph,
  parseCaption,
  parseDialogue,
  parseSfx
} from './parse';

function letteringNumbers(start = 1) {
  return new Sequence(start);
}

describe('parse lines', () => {
  describe('parseSpread', () => {
    test('single page', () => {
      const result = parseSpread('Page 2');

      expect(result).toEqual({
        type: 'spread',
        pageCount: 1,
        children: [],

        id: '',
        lineNumber: 0,
        startPage: 2,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });

    test('double page', () => {
      const result = parseSpread('Pages 2-3');

      expect(result).toEqual({
        type: 'spread',
        pageCount: 2,
        children: [],

        id: '',
        lineNumber: 0,
        startPage: 2,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });

    test('3+ page range', () => {
      const result = parseSpread('Pages 3-6');

      expect(result).toEqual({
        type: 'spread',
        pageCount: 4,
        children: [],

        id: '',
        lineNumber: 0,
        startPage: 3,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });

    test('partial page range', () => {
      const result = parseSpread('Pages 2-');

      expect(result).toEqual({
        type: 'spread',
        pageCount: 1,
        children: [],

        id: '',
        lineNumber: 0,
        startPage: 2,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });

    test('case insensitive', () => {
      const result = parseSpread('PAGE 1');

      expect(result).toEqual({
        type: 'spread',
        pageCount: 1,
        children: [],

        id: '',
        lineNumber: 0,
        startPage: 1,

        panelCount: 0,
        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0
      });
    });
  });

  describe('parsePanel', () => {
    test('single', () => {
      const result = parsePanel('Panel 3');

      expect(result).toEqual({
        type: 'panel',
        number: 3,
        children: [],
        description: '',

        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0,

        id: '',
        lineNumber: 0,
      });
    });

    test('case insensitive', () => {
      const result = parsePanel('PANEL 3');

      expect(result).toEqual({
        type: 'panel',
        number: 3,
        children: [],
        description: '',

        speakers: [],
        dialogueCount: 0,
        captionCount: 0,
        sfxCount: 0,
        dialogueWordCount: 0,
        captionWordCount: 0,

        id: '',
        lineNumber: 0,
      });
    });
  });

  describe('parseMetadata', () => {
    test('basic key/value', () => {
      const result = parseMetadata('Issue: 4');

      expect(result).toEqual({
        type: 'metadata',
        name: 'Issue',
        value: '4',

        id: '',
        lineNumber: 0,
      });
    });

    test('multi word key/value', () => {
      const result = parseMetadata('Written by: First Last');

      expect(result).toEqual({
        type: 'metadata',
        name: 'Written by',
        value: 'First Last',

        id: '',
        lineNumber: 0,
      });
    });
  });

  describe('parseParagraph', () => {
    test('plain text', () => {
      const result = parseParagraph('It was a good day.');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'text',
            content: 'It was a good day.'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('one bold word', () => {
      const result = parseParagraph('It was a **good** day.');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'text',
            content: 'It was a '
          },
          {
            type: 'bold-text',
            content: 'good'
          },
          {
            type: 'text',
            content: ' day.'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('multiple bold words', () => {
      const result = parseParagraph('It **was** a **good** day.');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'text',
            content: 'It '
          },
          {
            type: 'bold-text',
            content: 'was'
          },
          {
            type: 'text',
            content: ' a '
          },
          {
            type: 'bold-text',
            content: 'good'
          },
          {
            type: 'text',
            content: ' day.'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('mostly bold', () => {
      const result = parseParagraph('**It was** a **good day.**');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'bold-text',
            content: 'It was'
          },
          {
            type: 'text',
            content: ' a '
          },
          {
            type: 'bold-text',
            content: 'good day.'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('all bold', () => {
      const result = parseParagraph('**It was a good day.**');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'bold-text',
            content: 'It was a good day.'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('with url', () => {
      const result = parseParagraph('Looks like this https://example.com on page 1.');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'text',
            content: 'Looks like this '
          },
          {
            type: 'url-text',
            content: 'https://example.com'
          },
          {
            type: 'text',
            content: ' on page 1.'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('only a url', () => {
      const result = parseParagraph('https://example.com');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://example.com'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('dot after url', () => {
      const result = parseParagraph('https://example.com.');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://example.com'
          },
          {
            type: 'text',
            content: '.'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('url ending with /', () => {
      const result = parseParagraph('https://example.com/');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://example.com/'
          },
        ],

        id: '',
        lineNumber: 0,
      });
    });

    // The ? might be part of query string or the end of a question.
    // CW doesn't parse it as part of the url because leaving it out shouldn't
    // affect the response.
    test('? after url', () => {
      const result = parseParagraph('https://example.com?');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://example.com'
          },
          {
            type: 'text',
            content: '?'
          },
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('url ending with =', () => {
      const result = parseParagraph('https://example.com?name=');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://example.com?name='
          },
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('url ending with #', () => {
      const result = parseParagraph('https://example.com#');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://example.com#'
          },
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('multi url', () => {
      const result = parseParagraph('https://one.com http://two.com');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://one.com'
          },
          {
            type: 'text',
            content: ' '
          },
          {
            type: 'url-text',
            content: 'http://two.com'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('url inside bold', () => {
      const result = parseParagraph('**here https://one.com it is**');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'bold-text',
            content: 'here '
          },
          {
            type: 'url-text',
            content: 'https://one.com'
          },
          {
            type: 'bold-text',
            content: ' it is'
          },
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('bold url', () => {
      const result = parseParagraph('**https://one.com**');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://one.com'
          },
        ],

        id: '',
        lineNumber: 0,
      });
    });

    // not a valid use case but it's nice to know what happens
    test('invalid: bold inside url', () => {
      const result = parseParagraph('https://one**blah**two.com');

      expect(result).toEqual({
        type: 'paragraph',
        content: [
          {
            type: 'url-text',
            content: 'https://one'
          },
          {
            type: 'bold-text',
            content: 'blah'
          },
          {
            type: 'text',
            content: 'two.com'
          },
        ],

        id: '',
        lineNumber: 0,
      });
    });
  });

  describe('parseCaption', () => {
    test('basic', () => {
      const numbering = letteringNumbers(2);
      const result = parseCaption('\tCAPTION: It was a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 2,
        modifier: '',
        content: [{
          type: 'text',
          content: 'It was a good idea.'
        }],
        wordCount: 5,

        id: '',
        lineNumber: 0,
      });
    });

    test('with bold', () => {
      const numbering = letteringNumbers(5);
      const result = parseCaption('\tCAPTION: It **was** a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 5,
        modifier: '',
        content: [{
          type: 'text',
          content: 'It '
        }, {
          type: 'bold-text',
          content: 'was'
        }, {
            type: 'text',
            content: ' a good idea.'
        }],
        wordCount: 5,

        id: '',
        lineNumber: 0,
      });
    });

    test('with modifier', () => {
      const numbering = letteringNumbers(2);
      const result = parseCaption('\tCAPTION (PERSON): It was a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 2,
        modifier: 'PERSON',
        content: [{
          type: 'text',
          content: 'It was a good idea.'
        }],
        wordCount: 5,

        id: '',
        lineNumber: 0,
      });
    });

    test('with empty modifier parens', () => {
      const numbering = letteringNumbers(2);
      const result = parseCaption('\tCAPTION (): It was a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 2,
        modifier: '',
        content: [{
          type: 'text',
          content: 'It was a good idea.'
        }],
        wordCount: 5,

        id: '',
        lineNumber: 0,
      });
    });

    test('case insensitive', () => {
      const numbering = letteringNumbers(2);
      const result = parseCaption('\tcaption: It was a good idea.', numbering);

      expect(result).toEqual({
        type: 'caption',
        number: 2,
        modifier: '',
        content: [{
          type: 'text',
          content: 'It was a good idea.'
        }],
        wordCount: 5,

        id: '',
        lineNumber: 0,
      });
    });

  });

  describe('parseDialogue', () => {
    test('basic', () => {
      const numbering = letteringNumbers(2);
      const result = parseDialogue('\tBEAU: I thought it was a good idea.', numbering);

      expect(result).toEqual({
        type: 'dialogue',
        number: 2,
        speaker: 'BEAU',
        modifier: '',
        content: [{
          type: 'text',
          content: 'I thought it was a good idea.'
        }],
        wordCount: 7,

        id: '',
        lineNumber: 0,
      });
    });

    test('with bold', () => {
      const numbering = letteringNumbers(2);
      const result = parseDialogue('\tBEAU: I thought it was a **good** idea.', numbering);

      expect(result).toEqual({
        type: 'dialogue',
        number: 2,
        speaker: 'BEAU',
        modifier: '',
        content: [{
          type: 'text',
          content: 'I thought it was a '
        }, {
          type: 'bold-text',
          content: 'good'
        }, {
          type: 'text',
          content: ' idea.'
        }],
        wordCount: 7,

        id: '',
        lineNumber: 0,
      });
    });

    test('with modifier', () => {
      const numbering = letteringNumbers(2);
      const result = parseDialogue('\tBEAU (OFF): I thought it was a good idea.', numbering);

      expect(result).toEqual({
        type: 'dialogue',
        number: 2,
        speaker: 'BEAU',
        modifier: 'OFF',
        content: [{
          type: 'text',
          content: 'I thought it was a good idea.'
        }],
        wordCount: 7,

        id: '',
        lineNumber: 0,
      });
    });

    test('with empty modifier parens', () => {
      const numbering = letteringNumbers(2);
      const result = parseDialogue('\tBEAU (): I thought it was a good idea.', numbering);

      expect(result).toEqual({
        type: 'dialogue',
        number: 2,
        speaker: 'BEAU',
        modifier: '',
        content: [{
          type: 'text',
          content: 'I thought it was a good idea.'
        }],
        wordCount: 7,

        id: '',
        lineNumber: 0,
      });
    });
  });

  describe('parseSfx', () => {
    test('basic', () => {
      const numbering = letteringNumbers(2);
      const result = parseSfx('\tSFX: BLAM', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 2,
        modifier: '',
        content: [
          {
            type: 'text',
            content: 'BLAM'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('with modifier', () => {
      const numbering = letteringNumbers(5);
      const result = parseSfx('\tSFX (GLASS): clink', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 5,
        modifier: 'GLASS',
        content: [
          {
            type: 'text',
            content: 'clink'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('with empty modifier parens', () => {
      const numbering = letteringNumbers(5);
      const result = parseSfx('\tSFX (): clink', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 5,
        modifier: '',
        content: [
          {
            type: 'text',
            content: 'clink'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('case insensitive', () => {
      const numbering = letteringNumbers(12);
      const result = parseSfx('\tsfx: BLAM', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 12,
        modifier: '',
        content: [
          {
            type: 'text',
            content: 'BLAM'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('all bold', () => {
      const numbering = letteringNumbers(5);
      const result = parseSfx('\tSFX: **BLAM**', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 5,
        modifier: '',
        content: [
          {
            type: 'bold-text',
            content: 'BLAM'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });

    test('partial bold', () => {
      const numbering = letteringNumbers(5);
      const result = parseSfx('\tSFX: BA-**BOOM**', numbering);

      expect(result).toEqual({
        type: 'sfx',
        number: 5,
        modifier: '',
        content: [
          {
            type: 'text',
            content: 'BA-'
          },
          {
            type: 'bold-text',
            content: 'BOOM'
          }
        ],

        id: '',
        lineNumber: 0,
      });
    });
  });
});