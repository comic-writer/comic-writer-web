import * as classifiers from './line-classifiers';

describe('line classifiers', () => {
  describe('isBlank', () => {
    test('empty string', () => {
      const result = classifiers.isBlank('');

      expect(result).toBeTruthy();
    });

    test('spaces', () => {
      const result = classifiers.isBlank('  ');

      expect(result).toBeTruthy();
    });

    test('tab character', () => {
      const result = classifiers.isBlank('\t');

      expect(result).toBeTruthy();
    });

    test('non-whitespace', () => {
      const result = classifiers.isBlank('something');

      expect(result).toBeFalsy();
    });
  });
});
