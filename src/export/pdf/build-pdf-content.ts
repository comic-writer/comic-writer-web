import { Content, ContentText } from 'pdfmake/interfaces';
import {
  TextChunk,
  PreSpread,
  Paragraph,
  Metadata,
  Lettering,
  Sfx,
  Dialogue,
  Caption,
  Panel,
  PanelChild,
  SpreadChild,
  Spread,
} from '../../parser/nodes';

import { FullScript } from '../../script/types';
import * as headlineLevels from './headline-levels';
import * as parts from '../../comic-part-types';

import {
  normalizePanelChildren,
  normalizePreSpread,
  normalizeSpreadChildren,
} from '../common/normalize-blank-lines';
import { KnownMetadata } from '../../script/known-metadata';
import { spreadTitle } from '../common/spread-title';
import { extractComicTitleMetadata } from '../common/extract-title-metadata';
import { extractPanelDescription } from '../common/extract-panel-description';

export function buildPdfContent(script: FullScript): Content {
  return renderPreSpread(script.preSpread).concat(
    script.spreads.flatMap((spread) => renderSpread(spread))
  );
}

function renderPreSpread(preSpread: Array<PreSpread>): Array<Content> {
  const normalized = normalizePreSpread(preSpread);
  const { metadata, nodes } = extractComicTitleMetadata(normalized);

  return renderTitle(metadata).concat(
    nodes
      .map((node) => {
        switch (node.type) {
          case parts.METADATA:
            return renderMetadata(node);
          case parts.PARAGRAPH:
            return renderParagraph(node);
          case parts.BLANK:
            return renderBlankLine();
          default:
            const exhaustiveCheck: never = node;
            throw new Error(`Unhandled pre spread node: ${exhaustiveCheck}`);
        }
      })
      .map((node, index, array) => {
        const last = index === array.length - 1;
        return last ? lastPreSpread(node) : node;
      })
  );
}

function lastPreSpread(content: ContentText): ContentText {
  const oldStyle = content.style;
  const newStyle =
    typeof oldStyle === 'string'
      ? [oldStyle, 'lastPreSpread']
      : Array.isArray(oldStyle)
      ? oldStyle.concat('lastPreSpread')
      : 'lastPreSpread';

  return {
    ...content,
    style: newStyle,
  };
}

function renderTitle(metadata: KnownMetadata): Array<Content> {
  const content: Array<Content> = [];

  if (metadata.hasTitle) {
    const issue = metadata.hasIssue ? ` #${metadata.issue}` : '';

    content.push({
      text: `${metadata.title}${issue}`,
      style: 'title',
    });

    if (metadata.hasBy) {
      content.push(renderBlankLine());

      content.push({
        text: `By ${metadata.by}`,
        style: 'by',
      });
    }

    content.push(renderBlankLine());
  }

  return content;
}

function renderSpread(spread: Spread): Array<Content> {
  const content: Array<Content> = [];

  content.push({
    text: spreadTitle(spread),
    style: 'spread',
    headlineLevel: headlineLevels.SPREAD,
  });

  content.push(...renderSpreadChildren(spread.children));

  return content;
}

function renderSpreadChildren(nodes: Array<SpreadChild>): Array<Content> {
  return normalizeSpreadChildren(nodes).flatMap((node) => {
    switch (node.type) {
      case parts.PARAGRAPH:
        return renderParagraph(node);
      case parts.BLANK:
        return renderBlankLine();
      case parts.PANEL:
        return renderPanel(node);
      case parts.DIALOGUE:
        return renderDialogue(node);
      case parts.CAPTION:
        return renderCaption(node);
      case parts.SFX:
        return renderSfx(node);
      default:
        const exhaustiveCheck: never = node;
        throw new Error(`Unhandled spread child node: ${exhaustiveCheck}`);
    }
  });
}

function renderPanel(node: Panel): Array<Content> {
  const content: Array<Content> = [];

  const { description, remainingChildren } = extractPanelDescription(
    normalizePanelChildren(node.children)
  );

  content.push({
    text: panelText(node, description),
  });

  for (const child of remainingChildren) {
    content.push(renderPanelChild(child));
  }

  // placeholder for panels with no lettering
  if (node.letteringCount === 0) {
    content.push(renderBlankLine());
    content.push(renderRawParagraph('NO COPY'));
  }

  return content;
}

function panelText(panel: Panel, description: Paragraph | null): Content {
  if (description) {
    return [
      {
        text: `${panel.label}: `,
        style: 'panel',
        headlineLevel: headlineLevels.PANEL,
      },
      {
        text: paragraphContentToText(description.content),
      },
    ];
  } else {
    return {
      text: panel.label,
      style: 'panel',
      headlineLevel: headlineLevels.PANEL,
    };
  }
}

function renderPanelChild(node: PanelChild): Content {
  switch (node.type) {
    case parts.PARAGRAPH:
      return renderParagraph(node);
    case parts.BLANK:
      return renderBlankLine();
    case parts.DIALOGUE:
      return renderDialogue(node);
    case parts.CAPTION:
      return renderCaption(node);
    case parts.SFX:
      return renderSfx(node);
    default:
      const exhaustiveCheck: never = node;
      throw new Error(`Unhandled panel child node: ${exhaustiveCheck}`);
  }
}

function renderBlankLine(): ContentText {
  return {
    text: '\n',
  };
}

function renderParagraph(node: Paragraph): ContentText {
  return {
    text: paragraphContentToText(node.content),
  };
}

function renderRawParagraph(text: string): ContentText {
  return {
    text,
  };
}

function renderMetadata(node: Metadata): ContentText {
  return {
    text: `${node.name}: ${node.value}`,
  };
}

function renderLettering(subject: string, node: Lettering): Content {
  const modifier = node.modifier ? ` (${node.modifier})` : '';

  return {
    layout: 'noBorders',
    table: {
      widths: [200, '*'],
      dontBreakRows: true,
      body: [
        [
          {
            text: `${node.number}. ${subject}${modifier}:`,
          },
          {
            text: letteringContentToText(node.content),
          },
        ],
      ],
    },
  };
}

function paragraphContentToText(chunks: Array<TextChunk>) {
  return chunks.map((chunk) => ({
    text: chunk.content,
    style: chunk.isBold ? 'bold' : chunk.isUrl ? 'link' : undefined,
    link: chunk.isUrl ? chunk.content : undefined,
  }));
}

function letteringContentToText(chunks: Array<TextChunk>) {
  return chunks.map((chunk) => ({
    text: chunk.content,
    style: chunk.isBold ? 'lettering-bold' : undefined,
  }));
}

function renderDialogue(node: Dialogue): Content {
  return renderLettering(node.speaker, node);
}

function renderCaption(node: Caption): Content {
  return renderLettering('CAPTION', node);
}

function renderSfx(node: Sfx): Content {
  return renderLettering('SFX', node);
}
