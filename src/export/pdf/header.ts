import { FullScript } from '../../script/types';
import { Content } from 'pdfmake/interfaces';
import { headerText } from '../common/header-text';

export function createHeader(script: FullScript) {
  const text = headerText(script);

  return (currentPage: number): Content | undefined => {
    if (currentPage !== 1 && text) {
      return {
        text,
        margin: [72, 36, 72, 0],
        style: 'header'
      };
    }

    return undefined;
  };
}
