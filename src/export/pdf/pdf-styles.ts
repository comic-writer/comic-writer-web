import { Style, StyleDictionary } from 'pdfmake/interfaces';
import * as colors from '../common/colors';
import { ExportSettings } from '../settings';

/*
Sizes are in points, 1 point == 1/72 inch

https://pdfmake.github.io/docs/0.1/document-definition-object/styling/
*/

export function styles(settings: ExportSettings): StyleDictionary {
  return {
    // title at top of script: My Comic #1
    title: {
      fontSize: 20,
      bold: true,
      alignment: 'center',
      decoration: 'underline',
      lineHeight: 1,
    },
    // line right below title: By <writer name>
    by: {
      alignment: 'center'
    },
    // add some bottom margin to the last pre-spread node to create some space
    // line between it and the first spread
    lastPreSpread: {
      // [left, top, right, bottom]
      margin: [0, 0, 0, 12]
    },
    spread: {
      fontSize: 20,
      lineHeight: 1,
      bold: true
    },
    panel: {
      bold: true
    },
    // bold text in lettering
    'lettering-bold': {
      bold: settings.boldLetteringIsBold,
      decoration: settings.boldLetteringIsUnderlined ? 'underline' : undefined
    },
    // regular bold text, like in a paragraph
    bold: {
      bold: true
    },
    link: {
      decoration: 'underline',
      color: `#${colors.LINK}`
    },
    // text at top of every page except page 1
    header: {
      color: `#${colors.HEADER_TEXT}`
    },
  };
}

export function defaultStyle(fontName: string): Style {
  return {
    font: fontName,
    lineHeight: 1.2,
    fontSize: 12,
    color: `#${colors.DEFAULT_TEXT}`
  };
}
