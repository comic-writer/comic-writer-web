import * as pdfMake from "pdfmake/build/pdfmake";
import { TDocumentDefinitions } from 'pdfmake/interfaces';

import { FullScript } from '../../script/types';
import { buildPdfContent } from './build-pdf-content';
import { defaultStyle, styles } from './pdf-styles';
import { createHeader } from './header';
import * as headlineLevels from './headline-levels';

import RobotoBold from './Roboto-Bold.ttf';
import RobotoRegular from './Roboto-Regular.ttf';
import { ExportSettings } from '../settings';

const FONT_NAME = 'Roboto';

export function generatePdf(script: FullScript, settings: ExportSettings): Promise<Blob> {
  return new Promise(resolve => {
    const docDefinition = createDocDefinition(script, settings);
    const generator = pdfMake.createPdf(docDefinition, undefined, {
      [FONT_NAME]: {
        normal: toAbsolute(RobotoRegular),
        bold: toAbsolute(RobotoBold),
        // italics: _,
        // bolditalics: _
      }
    });

    generator.getBlob(blob => resolve(blob));
  });
}

function createDocDefinition(script: FullScript, settings: ExportSettings): TDocumentDefinitions {
  let spreadCount = 0;

  return {
    content: buildPdfContent(script),
    defaultStyle: defaultStyle(FONT_NAME),
    header: createHeader(script),
    pageMargins: 72,
    styles: styles(settings),
    pageBreakBefore(node, _followingNodesOnPage, _nodesOnNextPage, previousNodesOnPage) {
      if (node.headlineLevel === headlineLevels.SPREAD) {
        spreadCount += 1;

        if (spreadCount === 1 && node.startPosition.pageNumber === 1) {
          // break if first spread has been pushed more than 1/2 down page 1
          return node.startPosition.verticalRatio > 0.5;
        } else {
          // break if spread isn't first thing on page
          return previousNodesOnPage.length > 0;
        }
      }

      return false;
    }
  };
}

// pdfmake font loader only works with absolute urls
function toAbsolute(path: string): string {
  const url = new URL(window.location.href);
  url.pathname = path;
  return url.toString();
}