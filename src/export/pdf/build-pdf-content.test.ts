import { buildPdfContent } from './build-pdf-content';
import { generateTests } from '../common/snapshot-test-fixtures';

describe('buildPdfContent snapshots', () => {
  generateTests(buildPdfContent);
});
