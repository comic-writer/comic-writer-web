import { BlankLine, Paragraph, PreSpread, Metadata } from '../../parser/nodes';
import { extractComicTitleMetadata } from './extract-title-metadata';

describe('extractComicTitleMetadata', () => {
  test('with all comic title metadata', () => {
    const preSpread: Array<PreSpread> = [
      new BlankLine(),
      new Metadata('issue', '10'),
      new Metadata('By', 'John Doe'),
      new Metadata('TITLE', 'My Comic'),
    ];

    const result = extractComicTitleMetadata(preSpread);

    expect(result.metadata.title).toBe('My Comic');
    expect(result.metadata.issue).toBe('10');
    expect(result.metadata.by).toBe('John Doe');

    expect(result.nodes).toEqual([
      new BlankLine(),
    ]);
  });

  test('with all comic title metadata except title', () => {
    const preSpread: Array<PreSpread> = [
      new BlankLine(),
      new Metadata('issue', '10'),
      new Metadata('By', 'John Doe'),
    ];

    const result = extractComicTitleMetadata(preSpread);

    expect(result.metadata.title).toBe('Untitled');
    expect(result.metadata.issue).toBe('10');
    expect(result.metadata.by).toBe('John Doe');

    expect(result.nodes).toEqual([
      new BlankLine(),
    ]);
  });

  test('with no comic title metadata except title', () => {
    const preSpread: Array<PreSpread> = [
      new BlankLine(),
      new Metadata('Title', 'My Comic'),
    ];

    const result = extractComicTitleMetadata(preSpread);

    expect(result.metadata.title).toBe('My Comic');
    expect(result.metadata.hasIssue).toBeFalsy();
    expect(result.metadata.hasBy).toBeFalsy();

    expect(result.nodes).toEqual([
      new BlankLine(),
    ]);
  });

  test('with no comic title metadata', () => {
    const preSpread: Array<PreSpread> = [
      new Paragraph(),
      new BlankLine(),
      new Metadata('Year', '2021'),
    ];

    const result = extractComicTitleMetadata(preSpread);

    expect(result.metadata.title).toBe('Untitled');
    expect(result.metadata.hasIssue).toBeFalsy();
    expect(result.metadata.hasBy).toBeFalsy();

    expect(result.nodes).toEqual(preSpread);
  });

  test('with no metadata', () => {
    const preSpread: Array<PreSpread> = [
      new Paragraph(),
      new BlankLine(),
    ];

    const result = extractComicTitleMetadata(preSpread);

    expect(result.metadata.title).toBe('Untitled');
    expect(result.metadata.hasIssue).toBeFalsy();
    expect(result.metadata.hasBy).toBeFalsy();

    expect(result.nodes).toEqual(preSpread);
  });

  test('with no pre-spread nodes', () => {
    const preSpread: Array<PreSpread> = [];

    const result = extractComicTitleMetadata(preSpread);

    expect(result.metadata.title).toBe('Untitled');
    expect(result.metadata.hasIssue).toBeFalsy();
    expect(result.metadata.hasBy).toBeFalsy();

    expect(result.nodes.length).toBe(0);
  });

  test('does not mutate input array', () => {
    const preSpread: Array<PreSpread> = [];
    const copy = preSpread.slice();

    extractComicTitleMetadata(preSpread);

    expect(preSpread).toEqual(copy);
  });
});
