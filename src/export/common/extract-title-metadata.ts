import { PreSpread } from '../../parser/nodes';
import { KnownMetadata, isComicTitleMetadata } from '../../script/known-metadata';

/**
 * Pull comic title metadata out of the pre-spread nodes.
 *
 * @param nodes Pre-spread nodes
 * @returns Metadata and pre-spread nodes with the special title metadata nodes
 * removed, but only if there was enough metadata to make the title.
 */
export function extractComicTitleMetadata(nodes: Array<PreSpread>) {
  const metadata = KnownMetadata.create(nodes);

  return {
    metadata: metadata,
    nodes: nodes.filter(node =>
      !metadata.hasTitle || !isComicTitleMetadata(node)
    )
  };
}
