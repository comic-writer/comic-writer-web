import { BlankLine, PanelChild, Paragraph, Sfx, TextChunk } from '../../parser/nodes';
import { extractPanelDescription } from './extract-panel-description';

describe('extractPanelDescription', () => {
  test('given: <empty>', () => {
    const result = extractPanelDescription([]);

    expect(result.description).toBeNull();
    expect(result.remainingChildren).toEqual([]);
  });

  test('given: paragraph', () => {
    const children: Array<PanelChild> = [
      paragraph('blah'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toEqual(children[0]);
    expect(result.remainingChildren).toEqual([]);
  });

  test('given: non-paragraph', () => {
    const children: Array<PanelChild> = [
      sfx('BOOM'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toBeNull();
    expect(result.remainingChildren).toEqual(children);
  });

  test('given: paragraph, non-paragraph', () => {
    const children: Array<PanelChild> = [
      paragraph('blah'),
      sfx('BOOM'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toEqual(children[0]);
    expect(result.remainingChildren).toEqual(children.slice(1));
  });

  test('given: non-paragraph, paragraph', () => {
    const children: Array<PanelChild> = [
      sfx('BOOM'),
      paragraph('blah'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toBeNull();
    expect(result.remainingChildren).toEqual(children);
  });

  test('given: blank, paragraph', () => {
    const children: Array<PanelChild> = [
      new BlankLine(),
      paragraph('blah'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toEqual(children[1]);
    expect(result.remainingChildren).toEqual([]);
  });

  test('given: blank, blank, paragraph', () => {
    const children: Array<PanelChild> = [
      new BlankLine(),
      new BlankLine(),
      paragraph('blah'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toEqual(children[2]);
    expect(result.remainingChildren).toEqual([]);
  });

  test('given: blank, blank, non-paragraph', () => {
    const children: Array<PanelChild> = [
      new BlankLine(),
      new BlankLine(),
      sfx('BOOM'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toBeNull();
    expect(result.remainingChildren).toEqual(children);
  });

  test('given: blank, blank, paragraph, non-paragraph', () => {
    const children: Array<PanelChild> = [
      new BlankLine(),
      new BlankLine(),
      paragraph('blah'),
      sfx('BOOM'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toEqual(children[2]);
    expect(result.remainingChildren).toEqual(children.slice(3));
  });

  test('given: blank, blank, paragraph, paragraph', () => {
    const children: Array<PanelChild> = [
      new BlankLine(),
      new BlankLine(),
      paragraph('blah'),
      paragraph('second paragraph'),
    ];

    const result = extractPanelDescription(children);

    expect(result.description).toEqual(children[2]);
    expect(result.remainingChildren).toEqual(children.slice(3));
  });
});

function paragraph(text: string): Paragraph {
  const p = new Paragraph();
  p.content = [TextChunk.plain(text)];
  return p;
}

function sfx(effect: string): Sfx {
  const s = new Sfx();
  s.content = [TextChunk.plain(effect)];
  return s;
}
