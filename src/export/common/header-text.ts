import { FullScript } from '../../script/types';
import { KnownMetadata } from '../../script/known-metadata';

/**
 * @returns Text to use in page header or null if there is none
 */
export function headerText(script: FullScript) {
  return KnownMetadata.create(script.preSpread).headerText;
}
