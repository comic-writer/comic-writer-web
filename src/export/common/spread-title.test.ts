import { spreadTitle } from './spread-title';

describe('spreadTitle', () => {
  test('single page spread', () => {
    const title = spreadTitle({
      pageCount: 1,
      startPage: 2,
    });

    expect(title).toBe('TWO');
  });

  test('double page spread', () => {
    const title = spreadTitle({
      pageCount: 2,
      startPage: 2,
    });

    expect(title).toBe('TWO & THREE');
  });

  test('3+ page spread', () => {
    const title = spreadTitle({
      pageCount: 3,
      startPage: 2,
    });

    expect(title).toBe('TWO & THREE & FOUR');
  });

  test('high page number', () => {
    const title = spreadTitle({
      pageCount: 1,
      startPage: 21,
    });

    expect(title).toBe('TWENTY-ONE');
  });
});
