import { FullScript } from '../../script/types';
import { ScriptStore } from '../../script';

/*
 * Generates snapshot tests for the output of various export types.
 *
 * These snapshot tests aren't super nice but they do tell us that *something*
 * changed in the renderer output. Figuring out what changed, why, and if the
 * change matters is up to us.
 *
 * NOTE: This isn't currently used for docx tests because it resulted in 14 MB
 * of snapshots and it was impossible to read.
 */

type Renderer<T> = (script: FullScript) => T;

export function generateTests<T>(render: Renderer<T>): void {
  fixtures()
    .forEach(([name, rawScript]) => {
      // define a jest snapshot test
      test(name, () => {
        const result = render(parse(rawScript));
        expect(result).toMatchSnapshot();
      });
    });
}

function parse(script: string): FullScript {
  // This is probably a bad idea. Sorry future person.
  const store = new ScriptStore();
  store.replaceScript(script);
  return store.fullScript;
}

/**
 * `[<test name>, <raw script>]`
 */
function fixtures(): Array<[string, string]> {
  return [
    ['empty script', ''],
    [
      'pre-spread with title, issue, by',
      `Title: My Comic
Issue: 2
By: John Doe

Page 1`
    ],
    [
      'pre-spread with title and by',
      `Title: My Comic
By: John Doe

Page 1`
    ],
    [
      'pre-spread with title',
      `Title: My Comic

Page 1`
    ],
    [
      'pre-spread with no known metadata',
      `This is a pre spread paragraph.

Page 1`
    ],
    [
      'pre-spread with lots of blank lines',
      `This is first pre spread paragraph.




This is last pre spread paragraph.

Page 1`
    ],
    [
      'no pre-spread',
      `Page 1`
    ],
    [
      'basic spread with all lettering types',
      `Page 1
Panel 1

Bob walks in.

\tBOB: Hello I am Bob.

\tCAPTION: He was indeed.

\tSFX: BOOM`
    ],
    [
      'spread with lots of blank lines before panel',
      `Page 1




Panel 1

Bob walks in.

\tBOB: Hello I am Bob.`
    ],
    [
      'multi page spread',
      `Pages 1-2
Panel 1

Bob walks in.

\tBOB: Hello I am Bob.`
    ],
    [
      'multi spread',
      `Page 1
Panel 1

Bob walks in.

\tBOB: Hello I am Bob.

Page 2
Panel 1

Bob leaves.

\tBOB: See ya.`
    ],
    [
      // this is weird but it's currently supported
      'spread with lettering',
      `Page 1

\tBOB: Hello I am Bob.

\tCAPTION: He was indeed.

\tSFX: BOOM`
    ],
    [
      'bold in all types of lettering',
      `Page 1
Panel 1

Bob walks in.

\tBOB: Hello I am **Bob**.

\tCAPTION: He was **indeed**.

\tSFX: **BOOM**`
    ],
    [
      'bold in paragraphs',
      `Page 1
Panel 1

Bob **walks** in.

\tBOB: Hello I am Bob.`
    ],
    [
      'panel with no lettering',
      `Page 1
Panel 1

Bob walks in.`
    ],
    [
      'panel with no description',
      `Page 1
Panel 1

\tBOB: Hello I am Bob.`
    ],
    [
      'panel with multiple paragraphs before lettering',
      `Page 1
Panel 1

This is paragraph 1.

This is paragraph 2.

\tBOB: Hello I am Bob.`
    ],
    [
      'panel with lots of blank lines',
      `Page 1
Panel 1




Bob walks in.



\tBOB: Hello I am Bob.



\tCAPTION: He was indeed.




\tSFX: BOOM`
    ],
    [
      'panel with no blank lines',
      `Page 1
Panel 1
Bob walks in.
\tBOB: Hello I am Bob.
\tCAPTION: He was indeed.
\tSFX: BOOM`
    ]
  ];
}
