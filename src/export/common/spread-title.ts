import { Spread } from '../../parser/nodes';
import numberToWords from 'number-to-words';

/**
 * Type that has at least the specified properties of a base type.
 *
 * - Specified properties are required
 * - All other properties are optional
 */
type AtLeast<Base, RequiredKeys extends keyof Base> =
  Required<Pick<Base, RequiredKeys>> & Partial<Omit<Base, RequiredKeys>>;

type SpreadTitleProperties = AtLeast<Spread, 'pageCount' | 'startPage'>;

export function spreadTitle(spread: SpreadTitleProperties) {
  const pageNumbers = [];

  for (let i = 0; i < spread.pageCount; i++) {
    pageNumbers.push(spread.startPage + i);
  }

  return pageNumbers
    .map(number => numberToWords.toWords(number))
    .map(word => word.toUpperCase())
    .join(' & ');
}
