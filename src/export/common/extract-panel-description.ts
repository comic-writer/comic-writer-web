import * as parts from '../../comic-part-types';
import { PanelChild } from '../../parser/nodes';

/**
 * Extract the panel description, if it's there.
 */
export function extractPanelDescription(children: Array<PanelChild>) {
  const nonBlankIndex = children.findIndex(child => child.type !== parts.BLANK);
  const firstNonBlank = children[nonBlankIndex];

  if (firstNonBlank && firstNonBlank.type === parts.PARAGRAPH) {
    return {
      description: firstNonBlank,
      remainingChildren: children.slice(nonBlankIndex + 1)
    };
  } else {
    return {
      description: null,
      remainingChildren: children
    };
  }
}
