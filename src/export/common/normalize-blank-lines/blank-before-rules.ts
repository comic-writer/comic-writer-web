// Implementations of BlankBeforeCallback

import * as parts from '../../../comic-part-types';
import { PanelChild, PreSpread, SpreadChild } from '../../../parser/nodes';
import { BlankBeforeCallback } from './normalize-blanks';

export const preSpread: BlankBeforeCallback<PreSpread['type']> = (current, previous) => {
  switch (current.item) {
    case parts.METADATA: {
      return previous?.item === parts.PARAGRAPH;
    }
    case parts.PARAGRAPH: {
      if (previous?.item === parts.PARAGRAPH) {
        return current.blanksBefore > 0;
      } else {
        return !!previous;
      }
    }
  }

  return false;
}

export const spreadChildren: BlankBeforeCallback<SpreadChild['type']> = (current, previous) => {
  // it's the first child, space it out from spread heading
  if (!previous) return true;

  switch (current.item) {
    case parts.PARAGRAPH: {
      if (previous && previous.item === parts.PARAGRAPH) {
        return current.blanksBefore > 0;
      } else if (previous && parts.isLettering(previous.item)) {
        return true;
      }

      break;
    }
    case parts.CAPTION: // falls thru
    case parts.DIALOGUE: // falls thru
    case parts.SFX: {
      return !!previous && !parts.isLettering(previous.item);
    }
    case parts.PANEL: {
      return true;
    }
  }

  return false;
}

export const panelChildren: BlankBeforeCallback<PanelChild['type']> = (current, previous) => {
  switch (current.item) {
    case parts.PARAGRAPH: {
      if (previous && previous.item === parts.PARAGRAPH) {
        return current.blanksBefore > 0;
      } else if (previous && parts.isLettering(previous.item)) {
        return true;
      }
      break;
    }
    case parts.CAPTION: // falls thru
    case parts.DIALOGUE: // falls thru
    case parts.SFX: {
      return !!previous && !parts.isLettering(previous.item);
    }
  }

  return false;
}
