import {
  PreSpread,
  SpreadChild,
  PanelChild,
} from '../../../parser/nodes';
import * as rules from './blank-before-rules';

import {
  normalizeBlanks,
  rollUpBlanks,
} from './normalize-blanks';

/*
 * These functions accept comic nodes and return comic nodes that have blank
 * lines normalized. Normalizing blank lines is part of automatic formatting and
 * it helps the output will be uniform and predictable.
 *
 * In general this means
 *
 * - No consecutive blank lines
 * - Inserting blank lines where they improve readability
 */

export function normalizePreSpread(nodes: Array<PreSpread>): Array<PreSpread> {
  return normalizeBlanks(rollUpBlanks(nodes), rules.preSpread);
}

export function normalizeSpreadChildren(nodes: Array<SpreadChild>): Array<SpreadChild> {
  return normalizeBlanks(rollUpBlanks(nodes), rules.spreadChildren);
}

export function normalizePanelChildren(nodes: Array<PanelChild>): Array<PanelChild> {
  return normalizeBlanks(rollUpBlanks(nodes), rules.panelChildren);
}
