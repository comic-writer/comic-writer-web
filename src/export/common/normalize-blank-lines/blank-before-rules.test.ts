import * as parts from '../../../comic-part-types';
import { PanelChild, PreSpread, SpreadChild } from '../../../parser/nodes';
import * as rules from './blank-before-rules';
import { count } from './test-helpers';

describe('blank before rules', () => {
  test('preSpread', () => {
    const rule = rules.preSpread;

    expect(rule(count<PreSpread>(parts.METADATA, 0), undefined)).toBe(false);
    expect(rule(count<PreSpread>(parts.METADATA, 0), count<PreSpread>(parts.PARAGRAPH, 0))).toBe(true);
    expect(rule(count<PreSpread>(parts.METADATA, 0), count<PreSpread>(parts.METADATA, 0))).toBe(false);

    expect(rule(count<PreSpread>(parts.PARAGRAPH, 0), undefined)).toBe(false);
    expect(rule(count<PreSpread>(parts.PARAGRAPH, 1), count<PreSpread>(parts.PARAGRAPH, 0))).toBe(true);
    expect(rule(count<PreSpread>(parts.PARAGRAPH, 2), count<PreSpread>(parts.PARAGRAPH, 0))).toBe(true);
    expect(rule(count<PreSpread>(parts.PARAGRAPH, 0), count<PreSpread>(parts.PARAGRAPH, 0))).toBe(false);
    expect(rule(count<PreSpread>(parts.PARAGRAPH, 0), count<PreSpread>(parts.METADATA, 0))).toBe(true);
  });

  test('spreadChildren', () => {
    const rule = rules.spreadChildren;

    expect(rule(count<SpreadChild>(parts.PARAGRAPH, 0), undefined)).toBe(true);
    expect(rule(count<SpreadChild>(parts.PARAGRAPH, 1), count<SpreadChild>(parts.PARAGRAPH, 0))).toBe(true);
    expect(rule(count<SpreadChild>(parts.PARAGRAPH, 2), count<SpreadChild>(parts.PARAGRAPH, 0))).toBe(true);
    expect(rule(count<SpreadChild>(parts.PARAGRAPH, 0), count<SpreadChild>(parts.PARAGRAPH, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.PARAGRAPH, 0), count<SpreadChild>(parts.DIALOGUE, 0))).toBe(true);
    expect(rule(count<SpreadChild>(parts.PARAGRAPH, 0), count<SpreadChild>(parts.CAPTION, 0))).toBe(true);
    expect(rule(count<SpreadChild>(parts.PARAGRAPH, 0), count<SpreadChild>(parts.SFX, 0))).toBe(true);

    expect(rule(count<SpreadChild>(parts.CAPTION, 0), undefined)).toBe(true);
    expect(rule(count<SpreadChild>(parts.CAPTION, 0), count<SpreadChild>(parts.CAPTION, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.CAPTION, 0), count<SpreadChild>(parts.DIALOGUE, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.CAPTION, 0), count<SpreadChild>(parts.SFX, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.CAPTION, 0), count<SpreadChild>(parts.PARAGRAPH, 0))).toBe(true);

    expect(rule(count<SpreadChild>(parts.DIALOGUE, 0), undefined)).toBe(true);
    expect(rule(count<SpreadChild>(parts.DIALOGUE, 0), count<SpreadChild>(parts.CAPTION, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.DIALOGUE, 0), count<SpreadChild>(parts.DIALOGUE, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.DIALOGUE, 0), count<SpreadChild>(parts.SFX, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.DIALOGUE, 0), count<SpreadChild>(parts.PARAGRAPH, 0))).toBe(true);

    expect(rule(count<SpreadChild>(parts.SFX, 0), undefined)).toBe(true);
    expect(rule(count<SpreadChild>(parts.SFX, 0), count<SpreadChild>(parts.CAPTION, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.SFX, 0), count<SpreadChild>(parts.DIALOGUE, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.SFX, 0), count<SpreadChild>(parts.SFX, 0))).toBe(false);
    expect(rule(count<SpreadChild>(parts.SFX, 0), count<SpreadChild>(parts.PARAGRAPH, 0))).toBe(true);

    expect(rule(count<SpreadChild>(parts.PANEL, 0), undefined)).toBe(true);
    expect(rule(count<SpreadChild>(parts.PANEL, 0), count<SpreadChild>(parts.PARAGRAPH, 0))).toBe(true);
  });

  test('panelChildren', () => {
    const rule = rules.panelChildren;

    expect(rule(count<PanelChild>(parts.PARAGRAPH, 0), undefined)).toBe(false);
    expect(rule(count<PanelChild>(parts.PARAGRAPH, 1), count<PanelChild>(parts.PARAGRAPH, 0))).toBe(true);
    expect(rule(count<PanelChild>(parts.PARAGRAPH, 2), count<PanelChild>(parts.PARAGRAPH, 0))).toBe(true);
    expect(rule(count<PanelChild>(parts.PARAGRAPH, 0), count<PanelChild>(parts.PARAGRAPH, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.PARAGRAPH, 0), count<PanelChild>(parts.DIALOGUE, 0))).toBe(true);
    expect(rule(count<PanelChild>(parts.PARAGRAPH, 0), count<PanelChild>(parts.CAPTION, 0))).toBe(true);
    expect(rule(count<PanelChild>(parts.PARAGRAPH, 0), count<PanelChild>(parts.SFX, 0))).toBe(true);

    expect(rule(count<PanelChild>(parts.CAPTION, 0), undefined)).toBe(false);
    expect(rule(count<PanelChild>(parts.CAPTION, 0), count<PanelChild>(parts.CAPTION, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.CAPTION, 0), count<PanelChild>(parts.DIALOGUE, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.CAPTION, 0), count<PanelChild>(parts.SFX, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.CAPTION, 0), count<PanelChild>(parts.PARAGRAPH, 0))).toBe(true);

    expect(rule(count<PanelChild>(parts.DIALOGUE, 0), undefined)).toBe(false);
    expect(rule(count<PanelChild>(parts.DIALOGUE, 0), count<PanelChild>(parts.CAPTION, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.DIALOGUE, 0), count<PanelChild>(parts.DIALOGUE, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.DIALOGUE, 0), count<PanelChild>(parts.SFX, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.DIALOGUE, 0), count<PanelChild>(parts.PARAGRAPH, 0))).toBe(true);

    expect(rule(count<PanelChild>(parts.SFX, 0), undefined)).toBe(false);
    expect(rule(count<PanelChild>(parts.SFX, 0), count<PanelChild>(parts.CAPTION, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.SFX, 0), count<PanelChild>(parts.DIALOGUE, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.SFX, 0), count<PanelChild>(parts.SFX, 0))).toBe(false);
    expect(rule(count<PanelChild>(parts.SFX, 0), count<PanelChild>(parts.PARAGRAPH, 0))).toBe(true);

  });
});
