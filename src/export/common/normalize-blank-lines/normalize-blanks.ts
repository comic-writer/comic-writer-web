import { ComicNode, BlankLine } from '../../../parser/nodes';
import * as parts from '../../../comic-part-types';

/**
 * A comic node (or some value that represents the node) and the count of blank
 * lines between it and the previous item.
 */
export interface BlankCount<T> {
  item: T;
  blanksBefore: number;
}

export function rollUpBlanks<T extends ComicNode>(nodes: Array<T>) {
  const counts: Array<BlankCount<T>> = [];

  let consecutiveBlanks = 0;

  for (const node of nodes) {
    if (node.type === parts.BLANK) {
      consecutiveBlanks += 1;
    } else {
      counts.push({
        item: node,
        blanksBefore: consecutiveBlanks
      });

      consecutiveBlanks = 0;
    }
  }

  return counts;
}

/**
 * Takes the BlankCount for the current node and BlankCount for the previous
 * node (if any) and decides if the current node should have a blank line before
 * it.
 *
 * @param current
 * @param previous - Previous blank count, if any
 */
export type BlankBeforeCallback<T> = (current: BlankCount<T>, previous?: BlankCount<T>) => boolean;

const BLANK = new BlankLine();

export function normalizeBlanks<T extends ComicNode>(blankCounts: Array<BlankCount<T>>, needsBlankBefore: BlankBeforeCallback<T['type']>): Array<T | BlankLine> {
  return blankCounts
    .flatMap((current, index, array) => {
      const previous = index === 0 ? undefined : blanksWithTypeOf(array[index - 1]);

      return needsBlankBefore(blanksWithTypeOf(current), previous)
        ? [BLANK, current.item]
        : [current.item]
    });
}

function blanksWithTypeOf<T extends ComicNode>(count: BlankCount<T>): BlankCount<T['type']> {
  return {
    blanksBefore: count.blanksBefore,
    item: count.item.type
  };
}
