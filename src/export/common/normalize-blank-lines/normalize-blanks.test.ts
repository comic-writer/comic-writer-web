import {
  ComicNode,
  Paragraph,
  BlankLine,
  Metadata,
} from '../../../parser/nodes';
import * as parts from '../../../comic-part-types';

import {
  rollUpBlanks,
  normalizeBlanks,
  BlankCount,
} from './normalize-blanks';
import { count } from './test-helpers';

describe('rollUpBlanks', () => {
  test('no blanks since beginning', () => {
    const comicNodes: Array<ComicNode> = [
      new Paragraph(),
    ];

    const results = rollUpBlanks(comicNodes);

    expect(results.length).toBe(1);

    expect(results[0].blanksBefore).toBe(0);
    expect(results[0].item).toBe(comicNodes[0]);
  });

  test('one blank since beginning', () => {
    const comicNodes: Array<ComicNode> = [
      new BlankLine(),
      new Paragraph(),
    ];

    const results = rollUpBlanks(comicNodes);

    expect(results.length).toBe(1);

    expect(results[0].blanksBefore).toBe(1);
    expect(results[0].item).toBe(comicNodes[1]);
  });

  test('multiple blanks since beginning', () => {
    const comicNodes: Array<ComicNode> = [
      new BlankLine(),
      new BlankLine(),
      new Paragraph(),
    ];

    const results = rollUpBlanks(comicNodes);

    expect(results.length).toBe(1);

    expect(results[0].blanksBefore).toBe(2);
    expect(results[0].item).toBe(comicNodes[2]);
  });

  test('no blanks since previous node', () => {
    const comicNodes: Array<ComicNode> = [
      new Paragraph(),
      new Paragraph(),
    ];

    const results = rollUpBlanks(comicNodes);

    expect(results.length).toBe(2);

    expect(results[0].blanksBefore).toBe(0);
    expect(results[0].item).toBe(comicNodes[0]);

    expect(results[1].blanksBefore).toBe(0);
    expect(results[1].item).toBe(comicNodes[1]);
  });

  test('one blank since previous node', () => {
    const comicNodes: Array<ComicNode> = [
      new Paragraph(),
      new BlankLine(),
      new Paragraph(),
    ];

    const results = rollUpBlanks(comicNodes);

    expect(results.length).toBe(2);

    expect(results[0].blanksBefore).toBe(0);
    expect(results[0].item).toBe(comicNodes[0]);

    expect(results[1].blanksBefore).toBe(1);
    expect(results[1].item).toBe(comicNodes[2]);
  });

  test('multiple blanks since previous node', () => {
    const comicNodes: Array<ComicNode> = [
      new Paragraph(),
      new BlankLine(),
      new BlankLine(),
      new Paragraph(),
    ];

    const results = rollUpBlanks(comicNodes);

    expect(results.length).toBe(2);

    expect(results[0].blanksBefore).toBe(0);
    expect(results[0].item).toBe(comicNodes[0]);

    expect(results[1].blanksBefore).toBe(2);
    expect(results[1].item).toBe(comicNodes[3]);
  });
});

describe('normalizeBlanks', () => {
  test('invokes callback with current count and previous count, if any', () => {
    const blankCounts: Array<BlankCount<ComicNode>> = [
      {
        item: new Metadata('Year', '2021'),
        blanksBefore: 1
      },
      {
        item: new Metadata('issue', '3'),
        blanksBefore: 2
      }
    ];
    const callback = jest.fn();

    normalizeBlanks(blankCounts, callback);

    expect(callback).toHaveBeenCalledTimes(2);
    expect(callback).toHaveBeenNthCalledWith(1, count(parts.METADATA, 1), undefined);
    expect(callback).toHaveBeenNthCalledWith(2, count(parts.METADATA, 2), count(parts.METADATA, 1));
  });

  test('returns blank line before node when callback returns true', () => {
    const blankCounts: Array<BlankCount<ComicNode>> = [
      {
        item: new Metadata('issue', '3'),
        blanksBefore: 1
      }
    ];

    const results = normalizeBlanks(blankCounts, () => true);

    expect(results.length).toBe(2);
    expect(results[0].type).toBe(parts.BLANK);
    expect(results[1]).toEqual(blankCounts[0].item);
  });

  test('returns node when callback returns false', () => {
    const blankCounts: Array<BlankCount<ComicNode>> = [
      {
        item: new Metadata('issue', '3'),
        blanksBefore: 1
      }
    ];

    const results = normalizeBlanks(blankCounts, () => false);

    expect(results.length).toBe(1);
    expect(results[0]).toEqual(blankCounts[0].item);
  });
});
