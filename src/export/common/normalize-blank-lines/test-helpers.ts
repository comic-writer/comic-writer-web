import { ComicNode } from '../../../parser/nodes';
import { BlankCount } from './normalize-blanks';

/**
 * Make a BlankCount of a comic node type.
 */
export function count<T extends ComicNode>(type: T['type'], blanksBefore: number): BlankCount<T['type']> {
  return {
    blanksBefore,
    item: type
  };
}
