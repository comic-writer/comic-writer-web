/** Header at top of each page, without a leading # */
export const HEADER_TEXT = '2c2c2c';
/** Default text color, without a leading # */
export const DEFAULT_TEXT = '000000';
/** Link color, without a leading # */
export const LINK = '0366d6';
