import {
  Document,
  Paragraph,
  TextRun,
  ISectionOptions,
  Table,
  TableRow,
  TableCell,
  WidthType,
  TableBorders,
  HeadingLevel,
  Header,
  IParagraphOptions,
  ExternalHyperlink,
  UnderlineType,
} from 'docx';

import * as parts from '../../comic-part-types';
import {
  PreSpread,
  Paragraph as ComicParagraph,
  TextChunk,
  Metadata,
  Spread,
  SpreadChild,
  Panel,
  Dialogue,
  Caption,
  Sfx,
  PanelChild,
  Lettering,
} from '../../parser/nodes';
import { FullScript } from '../../script/types';

import { extractPanelDescription } from '../common/extract-panel-description';
import { extractComicTitleMetadata } from '../common/extract-title-metadata';
import { KnownMetadata } from '../../script/known-metadata';
import {
  normalizePanelChildren,
  normalizePreSpread,
  normalizeSpreadChildren,
} from '../common/normalize-blank-lines';
import { spreadTitle } from '../common/spread-title';
import { headerText } from '../common/header-text';
import { inchesToDxa, styles, paragraphStyles } from './docx-styles';
import { isKindOfBig } from './page-space-estimator';
import { ExportSettings } from '../settings';

/**
 * The element type of an array type.
 *
 * From https://stackoverflow.com/a/57447842/1865262
 */
type ArrayElement<ArrayType> = ArrayType extends readonly (infer T)[] ? T : never;

/**
 * Something that can be in a docx Section's children.
 */
type SectionChild = ArrayElement<ISectionOptions['children']>;

/**
 * Something that can be in a docx Paragraph's children.
 */
type ParagraphChild = ArrayElement<IParagraphOptions['children']>;

export function createRenderer(settings: ExportSettings) {
  // All the render functions are defined in this closure and so they all have
  // access to the settings object.

  return function render(script: FullScript): Document {
    const breakBeforeFirstSpread = isKindOfBig(script.preSpread);

    return new Document({
      sections: [
        {
          properties: {
            // This needs to be true to make `first` header option work but might not
            // be needed after https://github.com/dolanmiu/docx/issues/613
            titlePage: true,
          },
          headers: {
            first: new Header({
              // No header on first page
              children: []
            }),
            default: new Header({
              children: [renderHeader(script)]
            })
          },
          children: renderPreSpread(script.preSpread)
            // If there won't be a page break between pre-spread content and the
            // first spread, add a blank line between them so at least there's
            // some separation
            .concat(!breakBeforeFirstSpread ? [renderBlankLine()] : [])
            .concat(
              script.spreads.flatMap((spread, index) => {
                const pageBreakBefore = index !== 0 || breakBeforeFirstSpread;
                return renderSpread(spread, pageBreakBefore);
              })
            )
        }
      ],
      styles: styles()
    });
  }

  function renderHeader(script: FullScript): Paragraph {
    return new Paragraph({
      text: headerText(script) || undefined,
      style: paragraphStyles().PAGE_HEADER.id
    });
  }

  function renderPreSpread(preSpread: Array<PreSpread>): Array<SectionChild> {
    const normalized = normalizePreSpread(preSpread);

    const { metadata, nodes } = extractComicTitleMetadata(normalized);

    return renderComicTitle(metadata).concat(
      nodes.map((node) => {
        switch (node.type) {
          case parts.BLANK:
            return renderBlankLine();
          case parts.PARAGRAPH:
            return renderParagraph(node);
          case parts.METADATA:
            return renderMetadata(node);
          default:
            const exhaustiveCheck: never = node;
            throw new Error(`Unhandled pre-spread node: ${exhaustiveCheck}`);
        }
      })
    );
  }

  function renderComicTitle(metadata: KnownMetadata): Array<SectionChild> {
    const titleParagraphs: Array<Paragraph> = [];

    if (metadata.hasTitle) {
      const issue = metadata.hasIssue ? ` #${metadata.issue}` : '';

      titleParagraphs.push(
        new Paragraph({
          text: `${metadata.title}${issue}`,
          heading: HeadingLevel.TITLE,
        })
      );

      if (metadata.hasBy) {
        titleParagraphs.push(renderBlankLine());

        titleParagraphs.push(
          new Paragraph({
            text: `By: ${metadata.by}`,
            style: paragraphStyles().BY_WRITER_HEADING.id
          })
        );
      }

      titleParagraphs.push(renderBlankLine());
    }

    return titleParagraphs;
  }

  function renderParagraph(paragraph: ComicParagraph): Paragraph {
    return new Paragraph({
      children: renderParagraphContent(paragraph.content)
    });
  }

  function renderLetteringContent(chunks: Array<TextChunk>): Array<TextRun> {
    return chunks.map(
      (chunk) =>
        new TextRun({
          text: chunk.content,
          bold: chunk.isBold && settings.boldLetteringIsBold,
          underline:
            chunk.isBold && settings.boldLetteringIsUnderlined
              ? {
                  type: UnderlineType.SINGLE,
                }
              : undefined,
        })
    );
  }

  function renderParagraphContent(
    chunks: Array<TextChunk>
  ): Array<ParagraphChild> {
    return chunks.map((chunk) => {
      if (chunk.isBold) {
        return new TextRun({
          text: chunk.content,
          bold: true,
        });
      } else if (chunk.isPlain) {
        return new TextRun({
          text: chunk.content,
        });
      } else {
        return new ExternalHyperlink({
          child: new TextRun({
            text: chunk.content,
            style: 'Hyperlink',
          }),
          link: chunk.content,
        });
      }
    });
  }

  function renderMetadata(metadata: Metadata): Paragraph {
    return new Paragraph({
      text: `${metadata.name}: ${metadata.value}`,
      style: paragraphStyles().NORMAL.id
    });
  }

  function renderBlankLine(): Paragraph {
    // paragraph with empty string produces a single blank line
    return new Paragraph({
      text: '',
      style: paragraphStyles().NORMAL.id
    });
  }

  function renderSpread(
    spread: Spread,
    pageBreakBefore: boolean
  ): Array<SectionChild> {
    const spreadItems: Array<SectionChild> = [
      new Paragraph({
        text: spreadTitle(spread),
        heading: HeadingLevel.HEADING_1,
        pageBreakBefore
      })
    ];

    return spreadItems.concat(
      renderSpreadChildren(normalizeSpreadChildren(spread.children))
    );
  }

  function renderSpreadChildren(
    children: Array<SpreadChild>
  ): Array<SectionChild> {
    return children.flatMap((child) => {
      switch (child.type) {
        case parts.PARAGRAPH:
          return renderParagraph(child);
        case parts.BLANK:
          return renderBlankLine();
        case parts.PANEL:
          return renderPanel(child);
        case parts.DIALOGUE:
          return renderDialogue(child);
        case parts.CAPTION:
          return renderCaption(child);
        case parts.SFX:
          return renderSfx(child);
        default:
          const exhaustiveCheck: never = child;
          throw new Error(`Unhandled spread child node: ${exhaustiveCheck}`);
      }
    });
  }

  function renderPanel(panel: Panel): Array<SectionChild> {
    const { description, remainingChildren } = extractPanelDescription(
      normalizePanelChildren(panel.children)
    );

    const panelItems: Array<SectionChild> = [
      renderPanelText(panel, description),
      ...renderPanelChildren(remainingChildren)
    ];

    if (panel.letteringCount === 0) {
      panelItems.push(renderBlankLine());

      panelItems.push(
        new Paragraph({
          text: 'NO COPY',
          style: paragraphStyles().NORMAL.id
        })
      );
    }

    return panelItems;
  }

  function renderPanelText(
    panel: Panel,
    description: ComicParagraph | null
  ): Paragraph {
    const text: Array<ParagraphChild> = [];

    text.push(
      new TextRun({
        text: panel.label,
        bold: true,
        style: paragraphStyles().NORMAL.id
      })
    );

    if (description) {
      text.push(
        new TextRun({
          text: ':',
          bold: true,
          style: paragraphStyles().NORMAL.id
        })
      );

      text.push(
        new TextRun({
          text: ' ',
          style: paragraphStyles().NORMAL.id
        })
      );

      text.push(...renderParagraphContent(description.content));
    }

    return new Paragraph({
      children: text
    });
  }

  function renderPanelChildren(
    children: Array<PanelChild>
  ): Array<SectionChild> {
    return children.flatMap((child) => {
      switch (child.type) {
        case parts.PARAGRAPH:
          return renderParagraph(child);
        case parts.BLANK:
          return renderBlankLine();
        case parts.DIALOGUE:
          return renderDialogue(child);
        case parts.CAPTION:
          return renderCaption(child);
        case parts.SFX:
          return renderSfx(child);
        default:
          const exhaustiveCheck: never = child;
          throw new Error(`Unhandled panel child node: ${exhaustiveCheck}`);
      }
    });
  }

  function renderLettering(subject: string, node: Lettering): SectionChild {
    const modifier = node.modifier ? ` (${node.modifier})` : '';

    /** Overall width of table */
    const tableWidthInches = 6.5;
    /** Proportion of horizontal space given to left side of lettering */
    const metaPercent = 0.4;
    /** Proportion of horizontal space given to right side of lettering */
    const contentPercent = 0.6;
    /**
     * Extra space above lettering table cells.
     *
     * This doesn't seem to be true inches. I just tweaked the value until it
     * seemed good. Anything from 0.05 to 0.1 looks decent.
     */
    const cellTopMarginInches = 0.05;
    /**
     * Extra space below lettering table cells.
     *
     * This doesn't seem to be true inches. I just tweaked the value until it
     * seemed good. Anything from 0.05 to 0.1 looks decent.
     */
    const cellBottomMarginInches = 0.05;

    return new Table({
      width: {
        size: inchesToDxa(tableWidthInches),
        type: WidthType.DXA
      },
      columnWidths: [
        inchesToDxa(tableWidthInches * metaPercent),
        inchesToDxa(tableWidthInches * contentPercent)
      ],
      rows: [
        new TableRow({
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  text: `${node.number}. ${subject}${modifier}:`,
                  style: paragraphStyles().NORMAL.id
                }),
              ],
              width: {
                size: inchesToDxa(tableWidthInches * metaPercent),
                type: WidthType.DXA
              },
              borders: TableBorders.NONE,
              margins: {
                top: inchesToDxa(cellTopMarginInches),
                bottom: inchesToDxa(cellBottomMarginInches),
              }
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: renderLetteringContent(node.content),
                  style: paragraphStyles().NORMAL.id
                })
              ],
              width: {
                size: inchesToDxa(tableWidthInches * contentPercent),
                type: WidthType.DXA
              },
              borders: TableBorders.NONE,
              margins: {
                top: inchesToDxa(cellTopMarginInches),
                bottom: inchesToDxa(cellBottomMarginInches),
              }
            })
          ],
          // Disallow table rows from being split across 2 pages
          cantSplit: true
        }),
      ],
      borders: TableBorders.NONE
    });
  }

  function renderDialogue(dialogue: Dialogue) {
    return renderLettering(dialogue.speaker, dialogue);
  }

  function renderCaption(caption: Caption) {
    return renderLettering('CAPTION', caption);
  }

  function renderSfx(sfx: Sfx) {
    return renderLettering('SFX', sfx);
  }
}
