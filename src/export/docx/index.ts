import { Packer } from 'docx';
import { FullScript } from '../../script/types';
import { ExportSettings } from '../settings';
import { createRenderer } from './render';

export function generate(script: FullScript, settings: ExportSettings): Promise<Blob> {
  return Packer.toBlob(createRenderer(settings)(script));
}
