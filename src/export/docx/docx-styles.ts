import { AlignmentType, IStylesOptions, UnderlineType } from 'docx';
import * as colors from '../common/colors';

/**
 * Convert points to half points. Font sizes in docx are measured in half
 * points.
 *
 * 1 point == 1/72 inch. 1 half point == 1/144 inch.
 */
const pointsToHalfPoints = (points: number) => points * 2;

/**
 * Convert inches to DXA. DXA is also known as a twip. 1 DXA == 1/1440 inch.
 *
 * https://stackoverflow.com/a/14369133/1865262
 * @param inches
 */
export const inchesToDxa = (inches: number) => inches * 1440;

const FONT = 'Arial';

/**
 * High level styles for docx generation.
 */
export function styles(): IStylesOptions {
  return {
    default: {
      // comic title
      title: {
        paragraph: {
          alignment: AlignmentType.CENTER
        },
        run: {
          bold: true,
          size: pointsToHalfPoints(20),
          font: {
            name: FONT
          },
          color: colors.DEFAULT_TEXT,
          underline: {
            color: colors.DEFAULT_TEXT,
            type: UnderlineType.SINGLE
          }
        }
      },
      // spreads
      heading1: {
        paragraph: {
          alignment: AlignmentType.LEFT
        },
        run: {
          bold: true,
          size: pointsToHalfPoints(20),
          font: {
            name: FONT
          },
          color: colors.DEFAULT_TEXT
        }
      }
    },
    paragraphStyles: Object.values(paragraphStyles()),
  };
}

export function paragraphStyles() {
  return {
    // Plain text. Use this style if no other declared style makes more sense.
    NORMAL: {
      id: 'normal',
      name: 'Normal',
      run: {
        size: pointsToHalfPoints(12),
        font: {
          name: FONT
        },
        color: colors.DEFAULT_TEXT
      },
      paragraph: {
        alignment: AlignmentType.LEFT
      }
    },
    BY_WRITER_HEADING: {
      id: 'by',
      name: 'By',
      run: {
        size: pointsToHalfPoints(12),
        font: {
          name: FONT
        },
        color: colors.DEFAULT_TEXT
      },
      paragraph: {
        alignment: AlignmentType.CENTER
      }
    },
    // Short blurb at the top of each page
    PAGE_HEADER: {
      id: 'header',
      name: 'Header',
      run: {
        size: pointsToHalfPoints(12),
        font: {
          name: FONT
        },
        color: colors.HEADER_TEXT
      },
      paragraph: {
        alignment: AlignmentType.LEFT
      }
    }
  };
}
