import * as parts from '../../comic-part-types';
import { Paragraph, PreSpread } from '../../parser/nodes';
import { KnownMetadata } from '../../script/known-metadata';

// Current approximations
const CHARS_PER_LINE = 85;
const LINES_PER_PAGE = 50;
/**
 * Taking up more lines than this is "kind of big" for pre-spread content.
 */
const LINE_ALLOWANCE = LINES_PER_PAGE * 0.4;

/**
 * Estimates if the pre-spread nodes will take up enough space that the first
 * spread should start on a new page.
 *
 * ### Static factors that influence this
 *
 * - Font size
 * - Average character width
 * - Page margins
 * - Size of comic title and other headings
 */
export function isKindOfBig(preSpread: Array<PreSpread>): boolean {
  const blanks = preSpread.filter(node => node.type === parts.BLANK).length;
  const metadata = preSpread.filter(node => node.type === parts.METADATA).length;

  // These node types end with a newline
  let linesUsed = blanks + metadata;

  // "title" and "by" headings are a little bigger than regular text
  const knownMetadata = KnownMetadata.create(preSpread);
  if (knownMetadata.hasTitle) linesUsed += 1;
  if (knownMetadata.hasBy) linesUsed += 1;

  // paragraphs with a lot of text wrap to multi lines
  linesUsed += preSpread
    .filter((node): node is Paragraph => node.type === parts.PARAGRAPH)
    .reduce((count, paragraph) => count + estimateLines(paragraph), 0);

  return linesUsed > LINE_ALLOWANCE;
}

function estimateLines(paragraph: Paragraph): number {
  return Math.ceil(paragraph.toPlainText().length / CHARS_PER_LINE);
}
