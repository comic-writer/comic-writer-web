import * as slugify from './slugify';

describe('slugify', () => {
  [
    // default title
    ['Untitled', 'untitled'],
    ['untitled', 'untitled'],
    // case insensitive
    ['sUpErCoOl', 'supercool'],
    ['ALLCAPS', 'allcaps'],
    // multi word
    ['two words', 'two-words'],
    ['The Cool Comic', 'the-cool-comic'],
    ['   lots    of   spaces  ', 'lots-of-spaces'],
    // special characters
    ['Colon: The Comic', 'colon-the-comic'],
    ['Hack / Slash', 'hack-slash'],
    ['this & that', 'this-and-that'],
    ['already - has - hyphens', 'already-has-hyphens'],
    ['already-has-hyphens', 'already-has-hyphens'],
    ['jack.com', 'jack-dot-com'],
    ['../..', 'dot-dot-dot-dot'],
    ['./', 'dot'],
    ['_uncle_', 'uncle'],
    ['m@gic', 'm@gic'],
    ['Robert "Bob" Smith', 'robert-bob-smith'],
    ['This, that, and the other thing', 'this-that-and-the-other-thing'],
    ['bad < > : " / \ | ? * filename chars', 'bad-filename-chars'],
    ['--abc--', 'abc'],
    // titles
    ['Mr. Biscuit', 'mr-biscuit'],
    ['Ms. Biscuit', 'ms-biscuit'],
    ['Mrs. Biscuit', 'mrs-biscuit'],
    ['Dr. Biscuit', 'dr-biscuit'],
    // numbers
    ['12345', '12345'],
    ['Nanny 911', 'nanny-911'],
    // accented characters
    ['Über dude', 'über-dude'],
    ['el niño', 'el-niño'],
    ['Todo el día', 'todo-el-día'],
  ].forEach(([input, expected]) => {
    test(`forFilename("${input}") => "${expected}"`, () => {
      expect(slugify.forFilename(input)).toBe(expected);
    });
  });
});
