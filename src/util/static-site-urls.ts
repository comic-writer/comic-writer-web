export const privacyUrl = staticSite('/privacy-policy/');
export const guideUrl = staticSite('/guide/');
export const termsUrl = staticSite('/terms/');
export const blogUrl = staticSite('/blog/');
export const releaseNotesUrl = (fullVersion: string) => staticSite(`/blog/${fullVersion}`);

function staticSite(path = ''): string {
  return `https://about.comicwriter.io${path}`;
}
