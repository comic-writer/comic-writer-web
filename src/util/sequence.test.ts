import { Sequence } from './sequence';

describe('Sequence', () => {
  test('returns consecutive values from start', () => {
    const s = new Sequence(5);

    expect(s.next()).toBe(5);
    expect(s.next()).toBe(6);
    expect(s.next()).toBe(7);
  })
});
