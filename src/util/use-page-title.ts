import { useEffect } from 'react';

const DEFAULT_TITLE = 'ComicWriter';

export default function usePageTitle(title = DEFAULT_TITLE) {
  useEffect(() => {
    document.title = makeTitleString(title);
  }, [title]);
}

/**
 * Make the string to be used in the window title.
 *
 * @param title Title of the current page
 */
export function makeTitleString(title: string) {
  return title === DEFAULT_TITLE ? DEFAULT_TITLE : `${title} | ComicWriter`;
}
