// Detections yanked from codemirror
// https://github.com/codemirror/CodeMirror/blob/master/src/util/browser.js

const userAgent = navigator.userAgent;
const platform = navigator.platform;

export const mac = /Mac/.test(platform);
export const chromeOs = /\bCrOS\b/.test(userAgent);
export const windows = /win/i.test(platform);
