export default function prefersReducedMotion() {
  // Grab the prefers reduced media query.
  const mediaQuery = window.matchMedia("(prefers-reduced-motion: reduce)");
  // Check if the media query matches or is not available.
  return (!mediaQuery || mediaQuery.matches) ? true : false;
}
