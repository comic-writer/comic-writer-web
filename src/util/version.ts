export const version = process.env.REACT_APP_VERSION ?? '';

/**
 * Prefix of git hash that this is running.
 */
export const gitHash = (process.env.REACT_APP_REVISION ?? '').slice(0, 7);
