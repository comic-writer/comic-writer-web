import SettingsStore from './settings-store';
import { get, onSettingsChange } from './settings-api';

export { get, onSettingsChange } from './settings-api';
export type { SettingsStore };
export const settingsStore = new SettingsStore(get());

onSettingsChange(settings => {
  settingsStore._sync(settings)
});