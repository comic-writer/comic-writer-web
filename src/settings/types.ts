export type ComicWriterSettings = {
  /**
   * When true, errors are reported to an external, 3rd party error monitoring
   * service. When false, errors are not reported.
   */
  reportErrors: boolean,
  /**
   * How bold text in lettering is styled in exports.
   */
  boldLetteringExportStyle: BoldExportStyle,
  /**
   * When true, the browser's built-in spellcheck is enabled in the editor.
   */
  browserSpellcheck: boolean,
}

export type BoldExportStyle = 'bold' | 'underline' | 'bold-and-underline';
