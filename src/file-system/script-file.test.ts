import { FileSystemHandle } from 'browser-fs-access';
import {
  parseScriptFileContent,
  ScriptFile,
} from './script-file';

const dummyHandle: FileSystemHandle = {
  kind: 'file',
  name: 'comic.cwscript',
  isSameEntry: async () => false,
  queryPermission: async () => 'granted',
  requestPermission: async () => 'granted'
};

describe.only('script file', () => {
  test('ScriptFile#copy() does not copy the file handle', () => {
    const original = new ScriptFile();
    original.source = 'my comic';
    original.version = 1;
    original.handle = dummyHandle;

    const copy = original.copy();

    expect(copy.source).toBe(original.source);
    expect(copy.version).toBe(original.version);
    expect(copy.handle).toBeUndefined();
  });

  describe('parseScriptFileContent', () => {
    test('parses string into ScriptFileContent', () => {
      const string = JSON.stringify({
        source: 'story here',
        version: 1
      });
      const result = parseScriptFileContent(string);

      expect(result.source).toBe('story here');
      expect(result.version).toBe(1);
    });

    test('parses string without version into ScriptFileContent', () => {
      const string = JSON.stringify({
        source: 'script here'
      });
      const result = parseScriptFileContent(string);

      expect(result.source).toBe('script here');
      expect(result.version).toBe(1);
    });

    test('throws when given non-json', () => {
      expect(() => {
        parseScriptFileContent('asdf');
      }).toThrowError();
    });

    test('throws when given invalid json', () => {
      expect(() => {
        parseScriptFileContent('{source: "a", version: 1}');
      }).toThrowError();
    });

    test('throws when no top level object', () => {
      expect(() => {
        parseScriptFileContent('[1, 2, 3]');
      }).toThrowError();
    });

    test('throws when there is no source property', () => {
      expect(() => {
        parseScriptFileContent('{"version": 1}');
      }).toThrowError();
    });

    test('throws when source property is not a string', () => {
      expect(() => {
        parseScriptFileContent('{"source": true, "version": 1}');
      }).toThrowError();
    });
  });
});
