export type SpreadStat<T> = {
  /**
   * The page numbers spanned by this spread.
   *
   * length will always be >= 1
   */
  pages: Array<number>;
  value: T,
  /** Zero-based index of this stat among other stats of the same type */
  index: number,
  /** Zero-based line number for the spread */
  lineNumber: number,
}
