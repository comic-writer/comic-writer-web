import { bootUp } from '../src/boot-up';
import { RootStoreContext } from '../src/store/root-store-context';

export const decorators = [
  Story => {
    const store = bootUp({
      skipOnboarding: false,
      // All stories will have this script.
      // Indent matters for parsing.
      script: `title: My Comic
issue: 4
by: John Doe

Page 1
Panel 1
It's midnight and the city is empty.

Page 2
`
    });

    return (
      <RootStoreContext.Provider value={store}>
        <Story />
      </RootStoreContext.Provider>
    );
  }
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
};
