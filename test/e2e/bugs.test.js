import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getEditorLines, lettering, urlToWritingInterface } from './helpers';

fixture('bugs')
  .page(urlToWritingInterface());

test('any change in page after undoing deletion of page number', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'caption content')
    .pressKey('tab')
    // move back to top line
    .pressKey('up')
    .pressKey('up')
    .pressKey('up')
    // move over to after last char
    .pressKey('right')
    .pressKey('right')
    .pressKey('right')
    .pressKey('right')
    .pressKey('right')
    .pressKey('right')
    // delete page number
    .pressKey('backspace')
    // undo it
    .pressKey(shortcuts.undo)
    // arrow back down into the page and make an edit
    .pressKey('down')
    .pressKey('down')
    .pressKey('down')
    .typeText(selectors.editorInput(), 'a')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: caption content'),
      'a'
    ]);
});

test('hyphen after page number leaves cursor in correct spot', async t => {
  await t
    .typeText(selectors.editorInput(), 'pages 1-')
    .typeText(selectors.editorInput(), '2')
    .expect(getEditorLines()).eql([
      'pages 1-2'
    ])
});

test('panel and number with text after it is not a panel', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .typeText(selectors.editorInput(), 'panel 1 blah')
    // outline should not contain any panels
    .expect(selectors.outlineItem('1.').exists).notOk()
});

test('extra text after panel number should be preserved after enter is pressed', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .typeText(selectors.editorInput(), 'panel 1 blah')
    .pressKey('enter')

  await t.expect(getEditorLines()).eql([
    'Page 1',
    'panel 1 blah',
    ''
  ]);
});
