import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getSelectedText, lettering, urlToWritingInterface, getEditorLines } from './helpers';

fixture('dialogue - snippet behavior')
  .page(urlToWritingInterface());

test('splits line when triggered in middle of a line', async t => {
  await t
    .typeText(selectors.editorInput(), 'aabb')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.dialogue)
    .expect(getEditorLines()).eql([
      'aa',
      lettering('CHARACTER (): content'),
      'bb'
    ]);
});

test('moves line down when triggered at start of a line', async t => {
  await t
    .typeText(selectors.editorInput(), 'aabb')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.dialogue)
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (): content'),
      'aabb'
    ]);
});

test('starts a new line when triggered at end of a line', async t => {
  await t
    .typeText(selectors.editorInput(), 'aabb')
    .pressKey(shortcuts.dialogue)
    .expect(getEditorLines()).eql([
      'aabb',
      lettering('CHARACTER (): content'),
    ]);
});

test('after trigger, inserts dialogue template and activates snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (): content'),
    ])
    .expect(snippet.exists).ok();
});

test('after trigger, character placeholder is selected', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .expect(getSelectedText()).eql('CHARACTER');
});

test('character is auto-capitalized', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'joe')
    .expect(getEditorLines()).eql([
      lettering('JOE (): content'),
    ]);
});

test('modifier is auto-capitalized', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'off')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (OFF): content'),
    ]);
});

test('tabbing out of character puts cursor in modifier parens', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    // tab into modifier parens
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'OFF')
    // because cursor should be in modifier parens, typing will add a modifier
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (OFF): content'),
    ])
});

test('tabbing out of modifier without typing a modifier deletes parens and selects content placeholder', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    // tab into modifier parens
    .pressKey('tab')
    // tab into content
    .pressKey('tab')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER: content'),
    ])
    .expect(getSelectedText()).eql('content');
});

test('tabbing out of existing modifier selects content placeholder', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'OFF')
    .pressKey('tab')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (OFF): content'),
    ])
    .expect(getSelectedText()).eql('content');
});

test('tabbing from content deactivates snippet and puts cursor on next line', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    // tab to modifier
    .pressKey('tab')
    // tab to content
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'blah')
    // tab off of content
    .pressKey('tab')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER: blah'),
      ''
    ])
    .expect(snippet.exists).notOk()
    .typeText(selectors.editorInput(), 'on next line')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER: blah'),
      'on next line'
    ]);
});

test('shift-tabbing from content with no modifier brings back modifier parens and puts cursor in them', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    // tab to modifier
    .pressKey('tab')
    // tab to content
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'blah')
    // back to modifier
    .pressKey('shift+tab')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (): blah'),
    ])
    .typeText(selectors.editorInput(), 'OFF')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (OFF): blah'),
    ]);
});

test('shift-tabbing from content with a modifier selects the modifier', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    // tab into modifier
    .pressKey('tab')
    // add modifier
    .typeText(selectors.editorInput(), 'OFF')
    // tab to content
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'blah')
    // back to modifier
    .pressKey('shift+tab')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (OFF): blah'),
    ])
    .expect(getSelectedText()).eql('OFF');
});

test('shift-tabbing from modifier selects the character', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    // tab into modifier
    .pressKey('tab')
    // add modifier
    .typeText(selectors.editorInput(), 'OFF')
    // back to character
    .pressKey('shift+tab')
    .expect(getEditorLines()).eql([
      lettering('JOE (OFF): content'),
    ])
    .expect(getSelectedText()).eql('JOE');
});

test('shift-tabbing from character exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .pressKey('shift+tab')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (): content'),
    ])
    .expect(snippet.exists).notOk();
});

test('escape exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .pressKey('esc')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (): content'),
    ])
    .expect(snippet.exists).notOk();
});

test('arrow up exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey('enter')
    .pressKey(shortcuts.dialogue)
    // once to clear selection
    .pressKey('up')
    // again to actually move cursor
    .pressKey('up')
    .expect(getEditorLines()).eql([
      '',
      lettering('CHARACTER (): content'),
    ])
    .expect(snippet.exists).notOk();
});

test('arrow down exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey('enter')
    .pressKey('up')
    .pressKey(shortcuts.dialogue)
    // once to clear selection
    .pressKey('down')
    // again to actually move cursor
    .pressKey('down')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (): content'),
      ''
    ])
    .expect(snippet.exists).notOk();
});

test('moving to line start exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.dialogue)
    .pressKey('meta+left')
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (): content'),
    ])
    .expect(snippet.exists).notOk();
});
