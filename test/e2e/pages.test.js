import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getEditorLines, urlToWritingInterface } from './helpers';

fixture('pages')
  .page(urlToWritingInterface());

test('one page', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .expect(getEditorLines()).eql([
      'Page 1',
      ''
    ])
});

test('two pages', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.page)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Page 2',
      ''
    ])
});

test('insert page between existing pages', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.page)
    .pressKey('up')
    .pressKey('enter')
    .pressKey('up')
    .pressKey(shortcuts.page)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Page 2',
      '',
      'Page 3',
      ''
    ]);
});

test('one page range', async t => {
  await t
    .typeText(selectors.editorInput(), 'pages')
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Pages 1-2',
      ''
    ]);
});

test('two page ranges', async t => {
  await t
    .typeText(selectors.editorInput(), 'pages')
    .pressKey('enter')
    .typeText(selectors.editorInput(), 'pages')
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Pages 1-2',
      'Pages 3-4',
      ''
    ]);
});

test('insert page range between existing page ranges', async t => {
  await t
    .typeText(selectors.editorInput(), 'pages')
    .pressKey('enter')
    .typeText(selectors.editorInput(), 'pages')
    .pressKey('enter')
    .pressKey('up')
    .pressKey('enter')
    .pressKey('up')
    .typeText(selectors.editorInput(), 'pages')
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Pages 1-2',
      'Pages 3-4',
      '',
      'Pages 5-6',
      ''
    ]);
});

test('insert page range between existing single pages', async t => {
  await t
    .typeText(selectors.editorInput(), 'page')
    .pressKey('enter')
    .typeText(selectors.editorInput(), 'page')
    .pressKey('enter')
    .pressKey('up')
    .pressKey('enter')
    .pressKey('up')
    .typeText(selectors.editorInput(), 'pages')
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Pages 2-3',
      '',
      'Page 4',
      ''
    ]);
});
