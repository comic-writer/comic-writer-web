import * as selectors from '../selectors';
import * as shortcuts from '../keyboard-shortcuts';
import { getEditorLines, getSelectedText, urlToWritingInterface } from '../helpers';

fixture('bold shortcut - in paragraph - with selection')
  .page(urlToWritingInterface());

test('plain word fully selected', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      '**abc**',
    ])
    .expect(getSelectedText()).eql('abc')
});

test('plain word partially selected', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'a**bc**',
    ])
    .expect(getSelectedText()).eql('bc')
});

test('selection can contain plain whitespace', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'aa bb')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      '**aa bb**',
    ])
    .expect(getSelectedText()).eql('aa bb')
});

test('selection can contain plain punctuation', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'aa.bb')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      '**aa.bb**',
    ])
    .expect(getSelectedText()).eql('aa.bb')
});

test('plain word and bold word in a selection, no-op', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'a **b**')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'a **b**',
    ])
    .expect(getSelectedText()).eql('a **b**')
});

test('single bold token completely covered by selection, unbold entire token', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), '**bb**')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'bb',
    ])
    .expect(getSelectedText()).eql('bb')
});

test('single bold token partially covered by selection, unbold entire token', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), '**ab**')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'ab',
    ])
    .expect(getSelectedText()).eql('b')
});
