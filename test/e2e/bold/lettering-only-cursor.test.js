import * as selectors from '../selectors';
import * as shortcuts from '../keyboard-shortcuts';
import { getEditorLines, lettering, urlToWritingInterface } from '../helpers';

fixture('bold shortcut - in lettering - only a cursor')
  .page(urlToWritingInterface());

test('cursor in middle of regular word', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **abc**')
    ])
    // check that cursor was at **ab|c**
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **c**')
    ])
});

test('cursor at end of regular word', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'abc')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **abc**')
    ])
    // check that cursor was at **abc|**
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **a**')
    ])
});

test('cursor at start of regular word', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: ****abc')
    ])
    // hacky way to check that cursor was at: **|**abc
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **q**abc')
    ])
});

test('cursor in a regular word, bold stops at whitespace', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'a bbb a')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: a **bbb** a')
    ])
    // hacky way to check that cursor was at: a **bb|b** a
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: a **b** a')
    ])
});

test('cursor in a regular word, bold stops at punctuation', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'a.bbb.a')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: a.**bbb**.a')
    ])
    // hacky way to check that cursor was at: a.**bb|b**.a
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: a.**b**.a')
    ])
});

test('cursor in a regular word, bold stops at whitespace or punctuation', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'a bbb.a')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: a **bbb**.a')
    ])
    // hacky way to check that cursor was at: a **bb|b**.a
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: a **b**.a')
    ])
});

test('cursor in single word bold token, unbolds the entire token', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), '**abc**')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: abc')
    ])
    // hacky way to check that cursor was at: ab|c
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: c')
    ])
});

test('cursor in multi-word bold token, unbolds the entire token', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), '**first second**')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: first second')
    ])
    // hacky way to check that cursor was at: first secon|d
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: first secod')
    ])
});

test('cursor in opening stars of bold token, unbolds the entire token', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), '**abc**')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: abc')
    ])
    // hacky way to check that cursor was at: |abc
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: qabc')
    ])
});

test('cursor in closing stars of bold token, unbolds the entire token', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), '**abc**')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: abc')
    ])
    // hacky way to check that cursor was at: abc|
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: abcq')
    ])
});

test('cursor in whitespace, inserts 4 stars', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .pressKey('backspace')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: ****')
    ])
    // hacky way to check that cursor was at: **|**
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **q**')
    ])
});

test('cursor in whitespace near words, inserts 4 stars', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'one  two')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: one **** two')
    ])
    // hacky way to check that cursor was at: one **|** two
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: one **q** two')
    ])
});

// smoke test checking if bold also works in dialogue
test('also works in dialogue', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.dialogue)
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CHARACTER: **abc**')
    ])
    // check that cursor was at **ab|c**
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CHARACTER: **c**')
    ])
});

// smoke test checking if bold also works in sfx
test('also works in sfx', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.sfx)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('SFX: **abc**')
    ])
    // check that cursor was at **ab|c**
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('SFX: **c**')
    ])
});