import * as selectors from '../selectors';
import * as shortcuts from '../keyboard-shortcuts';
import { getEditorLines, urlToWritingInterface } from '../helpers';

fixture('unboldable scenarios')
  .page(urlToWritingInterface());

test('page line cannot be bolded', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey('up')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      '',
    ])
});

test('panel line cannot be bolded', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey('up')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      '',
    ])
});

test('metadata line cannot be bolded', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'key: value')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'key: value',
    ])
});

test('multi-line selections cannot be bolded', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'aaa')
    .pressKey('enter')
    .typeText(selectors.editorInput(), 'bbb')
    .pressKey('shift+up')
    .pressKey('shift+up')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'aaa',
      'bbb',
    ])
});
