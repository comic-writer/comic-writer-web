import * as selectors from '../selectors';
import * as shortcuts from '../keyboard-shortcuts';
import { getEditorLines, getSelectedText, lettering, urlToWritingInterface } from '../helpers';

fixture('bold shortcut - in lettering - with selection')
  .page(urlToWritingInterface());

test('plain word fully selected', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **abc**')
    ])
    .expect(getSelectedText()).eql('abc')
});

test('plain word partially selected', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'content')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: cont**ent**')
    ])
    .expect(getSelectedText()).eql('ent')
});

test('selection can include plain whitespace', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'aa bb')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **aa bb**')
    ])
    .expect(getSelectedText()).eql('aa bb')
});

test('selection can include plain punctuation', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'aa.bb')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: **aa.bb**')
    ])
    .expect(getSelectedText()).eql('aa.bb')
});

test('plain word and bold word in selection, no-op', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'a **b**')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: a **b**')
    ])
    .expect(getSelectedText()).eql('a **b**')
});

test('single bold token completely covered by selection, unbold entire token', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), '**abc**')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: abc')
    ])
    .expect(getSelectedText()).eql('abc')
});

test('single bold token partially covered by selection, unbold entire token', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), '**abc**')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: abc')
    ])
    .expect(getSelectedText()).eql('c')
});

test('selection partially to the left of the lettering colon, no-op', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'a')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CAPTION: a')
    ])
    .expect(getSelectedText()).eql('N: a')
});

// smoke test to make sure it works for dialogue
test('also works for dialogue', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.dialogue)
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('CHARACTER: **abc**')
    ])
    .expect(getSelectedText()).eql('abc')
});

// smoke test to make sure it works for sfx
test('also works for sfx', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.sfx)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey('shift+left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('SFX: **abc**')
    ])
    .expect(getSelectedText()).eql('abc')
});
