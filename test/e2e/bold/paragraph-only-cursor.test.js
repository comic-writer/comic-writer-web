import * as selectors from '../selectors';
import * as shortcuts from '../keyboard-shortcuts';
import { getEditorLines, urlToWritingInterface } from '../helpers';

fixture('bold shortcut - in paragraph - only a cursor')
  .page(urlToWritingInterface());

test('cursor in middle of regular word', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      '**abc**',
    ])
    // check that cursor was at **ab|c**
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      '**c**',
    ])
});

test('cursor at start of regular word', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'abc')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      '****abc',
    ])
    // check that cursor was at **|**abc
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      '**q**abc',
    ])
});

test('cursor at end of regular word', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'abc')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      '**abc**',
    ])
    // check that cursor was at **abc|**
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      '**ab**',
    ])
});

test('cursor in middle of regular word, bold stops at whitespace', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'a bbb c')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'a **bbb** c',
    ])
    // check that cursor was at a **bb|b** c
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'a **b** c',
    ])
});

test('cursor in middle of regular word, bold stops at punctuation', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'a.bbb.c')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'a.**bbb**.c',
    ])
    // check that cursor was at a.**bb|b**.c
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'a.**b**.c',
    ])
});

test('cursor in middle of regular word, bold stops at whitespace and punctuation', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'a bbb.c')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'a **bbb**.c',
    ])
    // check that cursor was at a **bb|b**.c
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'a **b**.c',
    ])
});

test('cursor in single word bold token, unbolds entire token', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), '**abc**')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'abc',
    ])
    // check that cursor was at ab|c
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'c',
    ])
});

test('cursor in multi word bold token, unbolds entire token', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), '**aaa bbb**')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'aaa bbb',
    ])
    // check that cursor was at aaa bb|b
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(getEditorLines()).eql([
      'aaa b',
    ])
});

test('cursor in opening stars of bold token, unbolds entire token', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), '**a**')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'a',
    ])
    // check that cursor was at |a
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      'qa',
    ])
});

test('cursor in closing stars of bold token, unbolds entire token', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), '**aa**')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'aa',
    ])
    // check that cursor was at aa|
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      'aaq',
    ])
});

test('cursor in whitespace, inserts bold stars', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      '****',
    ])
    // check that cursor was at **|**
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      '**q**',
    ])
});

test('cursor in near words, inserts bold stars', async t => {
  await t
    .click(selectors.editorInput())
    .typeText(selectors.editorInput(), 'aa  bb')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.bold)
    .expect(getEditorLines()).eql([
      'aa **** bb',
    ])
    // check that cursor was at aa **|** bb
    .typeText(selectors.editorInput(), 'q')
    .expect(getEditorLines()).eql([
      'aa **q** bb',
    ])
});
