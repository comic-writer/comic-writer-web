import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getEditorLines, urlToWritingInterface } from './helpers';

/**
 * Some basic tests to make sure preprocessing happens (and doesn't happen) at
 * the right times.
 */

fixture('preprocessing')
  .page(urlToWritingInterface());

test('preprocessing ignores line with cursor', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.page)
    .pressKey('up')
    .pressKey('up') // go back to position 0, 0
    .pressKey('enter')
    .pressKey('up')
    .typeText(selectors.editorInput(), 'page')
    .expect(getEditorLines()).eql([
      'page',
      'Page 2',
      'Page 3',
      ''
    ])
});

test('preprocessing updates line when cursor leaves by pressing enter', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.page)
    .pressKey('up')
    .pressKey('up') // back to position 0, 0
    .pressKey('enter')
    .pressKey('up')
    .typeText(selectors.editorInput(), 'page')
    .pressKey('enter') // cursor leaves by pressing enter
    .expect(getEditorLines()).eql([
      'Page 1',
      '',
      'Page 2',
      'Page 3',
      ''
    ])
});

test('preprocessing updates line when cursor leaves by using arrow', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.page)
    .pressKey('up')
    .pressKey('up') // back to position 0, 0
    .pressKey('enter')
    .pressKey('up')
    .typeText(selectors.editorInput(), 'page')
    .pressKey('down') // cursor leaves by using arrow
    .expect(getEditorLines()).eql([
      'Page 1',
      'Page 2',
      'Page 3',
      ''
    ])
});
