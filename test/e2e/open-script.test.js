import * as selectors from './selectors';
import * as helpers from './helpers';

fixture('open script')
  .page(helpers.urlToWritingInterface());

test('words counts are correct after opening script', async t => {
  await helpers.preloadBasicScript();

  await t
    .expect(selectors.wordCount(0).textContent).eql('4')
    .expect(selectors.wordCount(1).textContent).eql('3')
    .expect(selectors.wordCount(2).textContent).eql('3')
    .expect(selectors.wordCount(3).textContent).eql('1')
    .expect(selectors.wordCount(4).textContent).eql('1')
    .expect(selectors.wordCount(5).textContent).eql('1')
    .expect(selectors.wordCount(6).textContent).eql('1')
    .expect(selectors.wordCount(6).textContent).eql('1')
});

test('panel counts are correct after opening script', async t => {
  await helpers.preloadBasicScript();

  await t
    .expect(selectors.panelCount(0).textContent).eql('2 panels')
    .expect(selectors.panelCount(1).textContent).eql('1 panel')
});
