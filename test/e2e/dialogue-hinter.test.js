import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getSelectedText, lettering, urlToWritingInterface, getEditorLines } from './helpers';

fixture('dialogue - hinter popup')
  .page(urlToWritingInterface());

test('only appears when there are existing dialogues', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    // no popup yet
    .expect(selectors.letteringHintsPopup().exists).notOk()
    // give a character name
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    // now popup is shown
    .expect(selectors.letteringHintsPopup().exists).ok()
});

test('contains character names from all existing dialogues, in abc order', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    // popup is shown with existing character names
    .expect(selectors.letteringHintsItem(0).textContent).eql('ANNE')
    .expect(selectors.letteringHintsItem(1).textContent).eql('JOE')
    .expect(selectors.allLetteringHintItems().count).eql(2)
});

test('filters options as user types in character position', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'BOB')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JANET')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // 4th dialogue
    .pressKey(shortcuts.dialogue)
    // filter hint list
    .typeText(selectors.editorInput(), 'J')
    .expect(selectors.letteringHintsItem(0).textContent).eql('JANET')
    .expect(selectors.letteringHintsItem(1).textContent).eql('JOE')
    .expect(selectors.allLetteringHintItems().count).eql(2)
    // filter hint list even more
    .typeText(selectors.editorInput(), 'O')
    .expect(selectors.letteringHintsItem(0).textContent).eql('JOE')
    .expect(selectors.allLetteringHintItems().count).eql(1)
});

test('can filter down to one option and select it by pressing enter', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'A')
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('JOE: content'),
      lettering('ANNE: content'),
      lettering('ANNE (): content'),
    ]);
});

test('can arrow to an option and select it by pressing enter', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    .pressKey('down')
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('JOE: content'),
      lettering('ANNE: content'),
      lettering('JOE (): content'),
    ]);
});

test('can filter down to one option and select it by pressing tab', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'A')
    .pressKey('tab')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('JOE: content'),
      lettering('ANNE: content'),
      lettering('ANNE (): content'),
    ]);
});

test('can arrow to an option and select it by pressing tab', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    .pressKey('down')
    .pressKey('tab')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('JOE: content'),
      lettering('ANNE: content'),
      lettering('JOE (): content'),
    ]);
});

test('can filter down to one option and select it by pressing enter', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'A')
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('JOE: content'),
      lettering('ANNE: content'),
      lettering('ANNE (): content'),
    ]);
});

test('can select option by clicking it', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    .click(selectors.letteringHintsItem(1))
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('JOE: content'),
      lettering('ANNE: content'),
      lettering('JOE (): content'),
    ]);
});

test('disappears when escape is pressed', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    // popup is shown
    .expect(selectors.letteringHintsPopup().exists).ok()
    // hide popup
    .pressKey('esc')
    .expect(selectors.letteringHintsPopup().exists).notOk()
});

test('selection wraps when arrowing up through top of list', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    // go up to wrap to bottom of list
    .pressKey('up')
    // select bottom option
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('JOE: content'),
      lettering('ANNE: content'),
      lettering('JOE (): content'),
    ]);
});

test('selection wraps when arrowing down through bottom of list', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // first dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'JOE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // second dialogue
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'ANNE')
    .pressKey('tab')
    .pressKey('tab')
    .pressKey('tab')
    // third dialogue
    .pressKey(shortcuts.dialogue)
    // go down to wrap to top of list
    .pressKey('down')
    .pressKey('down')
    // select top option
    .pressKey('enter')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      lettering('JOE: content'),
      lettering('ANNE: content'),
      lettering('ANNE (): content'),
    ]);
});
