import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import * as helpers from './helpers';

fixture('outline')
  .page(helpers.urlToWritingInterface())
  .beforeEach(async t => {
    await t.resizeWindow(1024, 600);
  });

test('has Top item, even when script is blank', async t => {
  await t
    .expect(selectors.outlineItem('Top').count).eql(1)
});

test('each page gets an item in the outline', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.page)
    // 2 spread items and Top item
    .expect(selectors.allOutlineItems().count).eql(3)
    .expect(selectors.outlineItem(1).textContent).eql('Page 1')
    .expect(selectors.outlineItem(2).textContent).eql('Page 2')
});

test('each panel gets an item in the outline', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.panel)
    .expect(selectors.outlineItem('1.(no description)').count).eql(1)
    .expect(selectors.outlineItem('2.(no description)').count).eql(1)
});

test('panel description is shown in outline, if panel has one', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .typeText(selectors.editorInput(), 'It is a dark and stormy night.')
    .expect(selectors.outlineItem('1.It is a dark and stormy night.').count).eql(1);
});

test('only paragraphs before lettering are considered the panel description', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .typeText(selectors.editorInput(), 'before')
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'BOB')
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'blah blah blah')
    .pressKey('enter')
    .typeText(selectors.editorInput(), 'after')
    .expect(selectors.outlineItem('before').count).eql(1)
    .expect(selectors.outlineItem('before').textContent).eql('1.before')
});

test('clicking page in outline scrolls editor to that page', async t => {
  await helpers.preloadEvenSpacingScript();

  await t
    .click(selectors.outlineItem('Page 5'))
    // clicked item becomes current
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).eql('Page 5')
    // editor was scrolled to show page
    .expect(selectors.pageLine('Page 5').exists).ok()
    // To verify cursor location: type some text and see where it ends up
    .typeText(selectors.editorInput(), 'HERE')
    .expect(helpers.getEditorLines()).contains('Page 5HERE')
});

test('clicking panel in outline scrolls editor to that panel', async t => {
  await helpers.preloadEvenSpacingScript();

  await t
    .click(selectors.outlineItem('desc 4.3'))
    // clicked item becomes current
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).eql('3.desc 4.3')
    // editor was scrolled to show panel
    .expect(selectors.paragraphLine('desc 4.3').exists).ok()
    // To verify cursor location: type some text and see where it ends up
    .typeText(selectors.editorInput(), 'HERE')
    .expect(helpers.getEditorLines()).contains('Panel 3HERE')
});

// this was a bug from an earlier version of the outline
test('click an item, scroll editor away, click same item again puts editor back on the item', async t => {
  await helpers.preloadEvenSpacingScript();

  await t
    .click(selectors.outlineItem('Page 2'))
    // page visible after clicking page item
    .expect(selectors.pageLine('Page 2').exists).ok()

  await helpers.scrollEditorBy(2000);

  await t
    .click(selectors.outlineItem('Page 2'))
    // page visible after clicking page item again
    .expect(selectors.pageLine('Page 2').exists).ok()
});

test('current item changes as editor scrolls through pages and panels', async t => {
  await helpers.preloadEvenSpacingScript();

  await t
    .click(selectors.outlineItem('desc 1.4'))

  // panel 1.4 is current
  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('desc 1.4')

  await helpers.scrollEditorBy(60);

  // page 2 is current
  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('Page 2')

  await helpers.scrollEditorBy(60);

  // panel 2.1 is current
  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('desc 2.1')

  await helpers.scrollEditorBy(60);

  // panel 2.2 is current
  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('desc 2.2')

  await helpers.scrollEditorBy(60);

  // panel 2.3 is current
  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('desc 2.3')

  await helpers.scrollEditorBy(60);

  // panel 2.4 is current
  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('desc 2.4')
});

test('scrolling editor to bottom moves outline to bottom', async t => {
  await helpers.preloadEvenSpacingScript();

  // scroll down to the bottom
  await repeat(11, async () => await helpers.scrollEditorBy(3000));

  // let outline catch up to editor's jump to bottom
  await t.wait(500)

  // check current item
  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('desc 8.4')

  // check that outline is at bottom
  const isVisible = await helpers.isItemVisibleInOutline(selectors.outlineItem('Page 8'));
  await t.expect(isVisible).ok();
});

// similar to the "scroll to bottom" test except this uses keyboard
test('jumping editor to bottom moves outline to bottom', async t => {
  await helpers.preloadEvenSpacingScript();

  await t
    .click(selectors.editorInput())
    .pressKey('meta+down')
    // let outline catch up to editor's jump to bottom
    .wait(500)

  // check current item
  await t.expect(selectors.currentOutlineItem().count).eql(1);
  await t.expect(selectors.currentOutlineItem().textContent).contains('2.desc 7.2');

  // check that outline is at bottom
  const isVisible = await helpers.isItemVisibleInOutline(selectors.outlineItem('Page 8'));
  await t.expect(isVisible).ok();
});

test('scrolling editor to top moves outline to top', async t => {
  await helpers.preloadEvenSpacingScript();

  await t
    .click(selectors.editorInput())
    .pressKey('meta+down')

  // scroll back up incrementally because CM doesn't seem to detect when it
  // scrolls too much at once
  await repeat(11, async () => await helpers.scrollEditorBy(-3000));

  // let outline catch up to editor's scroll
  await t.wait(500)

  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('Top')

  // check that outline is at top
  const isVisible = await helpers.isItemVisibleInOutline(selectors.outlineItem('Page 1'));
  await t.expect(isVisible).ok();
});

// similar to the "scroll to top" test except this uses keyboard
test('jumping editor to top moves outline to top', async t => {
  await helpers.preloadEvenSpacingScript();

  await t
    .click(selectors.editorInput())
    .pressKey('meta+down')
    .pressKey('meta+up')
    // let outline catch up to editor's jump to top
    .wait(500)

  await t
    .expect(selectors.currentOutlineItem().count).eql(1)
    .expect(selectors.currentOutlineItem().textContent).contains('Top')

  // check that outline is at top
  const isVisible = await helpers.isItemVisibleInOutline(selectors.outlineItem('Page 1'));
  await t.expect(isVisible).ok();
});

test('clicking item near edge of viewport auto scrolls to get current item near middle of viewport', async t => {
  await helpers.preloadEvenSpacingScript();

  await t.click(selectors.outlineItem('desc 5.3'));

  // let outline catch up
  await t.wait(500)

  // Page 8 is now visible because outline auto-scrolled
  const isVisible = await helpers.isItemVisibleInOutline(selectors.outlineItem('3.desc 7.3'));
  await t.expect(isVisible).ok();

  // Top is no longer visible because outline auto-scrolled
  const topVisible = await helpers.isItemVisibleInOutline(selectors.outlineItem('Top'));
  await t.expect(topVisible).notOk();
});

test('current item near edge of viewport auto scrolls to get current item near middle of viewport', async t => {
  await helpers.preloadEvenSpacingScript();

  await t.click(selectors.outlineItem('desc 4.4'));

  await helpers.scrollEditorBy(20);

  // let outline catch up to editor
  await t.wait(500)

  // check that page 6 is now visible
  const isVisible = await helpers.isItemVisibleInOutline(selectors.outlineItem('Page 6'));
  await t.expect(isVisible).ok();

  // Top is no longer visible because outline auto-scrolled
  const topVisible = await helpers.isItemVisibleInOutline(selectors.outlineItem('Top'));
  await t.expect(topVisible).notOk();
});

async function repeat(times, fn) {
  for (let i = 0; i < times; i++) {
    await fn();
  }
}
