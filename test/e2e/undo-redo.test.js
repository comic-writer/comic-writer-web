import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getEditorLines, urlToWritingInterface } from './helpers';

fixture('undo / redo')
  .page(urlToWritingInterface());

test('add text then undo it', async t => {
  await t
    .typeText(selectors.editorInput(), 'this will be undone')
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      '',
    ])
});

test('delete text then undo it', async t => {
  await t
    .typeText(selectors.editorInput(), 'text')
    .pressKey('backspace')
    .pressKey('backspace')
    .pressKey('backspace')
    .pressKey('backspace')
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      'text',
    ]);
});

test('add text, undo, then redo', async t => {
  await t
    .typeText(selectors.editorInput(), 'this will come back')
    .pressKey(shortcuts.undo)
    .pressKey(shortcuts.redo)
    .expect(getEditorLines()).eql([
      'this will come back',
    ]);
});

test('undo 2 edits, then redo them', async t => {
  await t
    .typeText(selectors.editorInput(), 'first')
    .pressKey('enter')
    // pause to allow history event to be saved
    .wait(1300)
    .typeText(selectors.editorInput(), 'second')
    .pressKey(shortcuts.undo)
    .pressKey(shortcuts.undo)
    .pressKey(shortcuts.redo)
    .pressKey(shortcuts.redo)
    .expect(getEditorLines()).eql([
      'first',
      'second',
    ]);
});

test('extra redos have no effect', async t => {
  await t
    .typeText(selectors.editorInput(), 'first')
    .pressKey('enter')
    // pause to allow history event to be saved
    .wait(1300)
    .typeText(selectors.editorInput(), 'second')
    // undo one edit
    .pressKey(shortcuts.undo)
    // and redo the edit
    .pressKey(shortcuts.redo)
    // all these extra redos do nothing
    .pressKey(shortcuts.redo)
    .pressKey(shortcuts.redo)
    .pressKey(shortcuts.redo)
    .expect(getEditorLines()).eql([
      'first',
      'second',
    ]);
});

test('redo with no previous undo does nothing', async t => {
  await t
    .typeText(selectors.editorInput(), 'this is the line')
    .pressKey(shortcuts.redo)
    .expect(getEditorLines()).eql([
      'this is the line',
    ]);
});
