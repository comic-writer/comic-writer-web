import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getSelectedText, lettering, urlToWritingInterface, getEditorLines } from './helpers';

fixture('sfx')
  .page(urlToWritingInterface());

test('splits line when triggered in middle of a line', async t => {
  await t
    .typeText(selectors.editorInput(), 'aabb')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.sfx)
    .expect(getEditorLines()).eql([
      'aa',
      lettering('SFX (): content'),
      'bb'
    ]);
});

test('moves line down when triggered at start of a line', async t => {
  await t
    .typeText(selectors.editorInput(), 'aabb')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey('left')
    .pressKey(shortcuts.sfx)
    .expect(getEditorLines()).eql([
      lettering('SFX (): content'),
      'aabb'
    ]);
});

test('starts a new line when triggered at end of a line', async t => {
  await t
    .typeText(selectors.editorInput(), 'aabb')
    .pressKey(shortcuts.sfx)
    .expect(getEditorLines()).eql([
      'aabb',
      lettering('SFX (): content'),
    ]);
});

test('after trigger, inserts sfx template and activates snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    .expect(getEditorLines()).eql([
      lettering('SFX (): content'),
    ])
    .expect(snippet.exists).ok();
});

test('after trigger, cursor is inside modifier parens', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    .typeText(selectors.editorInput(), 'OFF')
    .expect(getEditorLines()).eql([
      lettering('SFX (OFF): content'),
    ]);
});

test('modifier is auto-capitalized', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    .typeText(selectors.editorInput(), 'off')
    .expect(getEditorLines()).eql([
      lettering('SFX (OFF): content'),
    ]);
});


test('after trigger, tabbing without typing a modifier deletes parens and selects content placeholder', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    .pressKey('tab')
    .expect(getEditorLines()).eql([
      lettering('SFX: content'),
    ])
    .expect(getSelectedText()).eql('content');
});

test('after trigger, tabbing with existing modifier selects content placeholder', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    .typeText(selectors.editorInput(), 'OFF')
    .pressKey('tab')
    .expect(getEditorLines()).eql([
      lettering('SFX (OFF): content'),
    ])
    .expect(getSelectedText()).eql('content');
});

test('tabbing from content deactivates snippet and puts cursor on next line', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    // tab to content
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'blah')
    // tab off of content
    .pressKey('tab')
    .expect(getEditorLines()).eql([
      lettering('SFX: blah'),
      ''
    ])
    .expect(snippet.exists).notOk()
    .typeText(selectors.editorInput(), 'on next line')
    .expect(getEditorLines()).eql([
      lettering('SFX: blah'),
      'on next line'
    ]);
});

test('shift-tabbing from content with no modifier brings back modifier parens and puts cursor in them', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    // tab to content
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'blah')
    // back to modifier
    .pressKey('shift+tab')
    .expect(getEditorLines()).eql([
      lettering('SFX (): blah'),
    ])
    .typeText(selectors.editorInput(), 'OFF')
    .expect(getEditorLines()).eql([
      lettering('SFX (OFF): blah'),
    ]);
});

test('shift-tabbing from content with existing modifier selects the modifier', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    // add modifier
    .typeText(selectors.editorInput(), 'OFF')
    // tab to content
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'blah')
    // back to modifier
    .pressKey('shift+tab')
    .expect(getEditorLines()).eql([
      lettering('SFX (OFF): blah'),
    ])
    .expect(getSelectedText()).eql('OFF');
});

test('shift-tabbing from modifier exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    .pressKey('shift+tab')
    .expect(getEditorLines()).eql([
      lettering('SFX: content'),
    ])
    .expect(snippet.exists).notOk();
});


test('escape exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    .pressKey('esc')
    .expect(getEditorLines()).eql([
      lettering('SFX (): content'),
    ])
    .expect(snippet.exists).notOk();
});

test('arrow up exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey('enter')
    .pressKey(shortcuts.sfx)
    // once to clear selection
    .pressKey('up')
    // again to actually move cursor
    .pressKey('up')
    .expect(getEditorLines()).eql([
      '',
      lettering('SFX (): content'),
    ])
    .expect(snippet.exists).notOk();
});

test('arrow down exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey('enter')
    .pressKey('up')
    .pressKey(shortcuts.sfx)
    // once to clear selection
    .pressKey('down')
    // again to actually move cursor
    .pressKey('down')
    .expect(getEditorLines()).eql([
      lettering('SFX (): content'),
      ''
    ])
    .expect(snippet.exists).notOk();
});

test('moving to line start exits lettering snippet', async t => {
  const snippet = selectors.activeLetteringSnippet();

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.sfx)
    .pressKey('meta+left')
    .expect(getEditorLines()).eql([
      lettering('SFX (): content'),
    ])
    .expect(snippet.exists).notOk();
});
