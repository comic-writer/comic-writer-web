import fs from 'fs';
import path from 'path';
import util from 'util';

import { ClientFunction } from 'testcafe';
import { Selector } from 'testcafe';
import * as selectors from './selectors';

const readFile = util.promisify(fs.readFile);

/**
 * Get all lines from the editor.
 */
export const getEditorLines = ClientFunction(() => {
  const collection = document.querySelectorAll(editorLineSelector);

  // Using old-style for loop due to limitations within ClientFunctions
  // https://devexpress.github.io/testcafe/documentation/guides/basic-guides/obtain-client-side-info.html#client-function-limitations
  const lineElements = [];
  for (let i = 0; i < collection.length; i++) {
    lineElements.push(collection[i]);
  }

  return lineElements
    .map(lineElement => lineElement.textContent || '')
    // CodeMirror uses zero-width space to represent blank lines.
    // Convert those to empty strings so assertions will be simpler.
    .map(text => text.replace(/\u{200B}/u, ''));
}, {
  dependencies: {
    editorLineSelector: selectors.css.editorLine
  }
});

export const getSelectedText = ClientFunction(() => {
  return window.getSelection().toString();
});

const LETTERING_INDENT = ' '.repeat(8);
export function lettering(lineAfterIndent) {
  return `${LETTERING_INDENT}${lineAfterIndent}`;
}

export async function preloadBitchPlanetScript() {
  await preloadScript('bitch-planet-3.cwscript');
}

export async function preloadEvenSpacingScript() {
  await preloadScript('even-spacing.cwscript');
}

export async function preloadBasicScript() {
  await preloadScript('basic.cwscript');
}

async function preloadScript(filename) {
  const filePath = path.resolve(__dirname, '../../sample-scripts', filename);
  const fileContents = await readFile(filePath, { encoding: 'utf8' });
  const json = JSON.parse(fileContents);
  const script = json.source;

  await ClientFunction(() => window.openScript(script), {
    dependencies: {
      script
    }
  })();
}

export async function scrollEditorBy(yDelta) {
  return await ClientFunction(() => {
    document.querySelector('.CodeMirror-scroll').scrollBy(0, yDelta);
  }, {
    dependencies: {
      yDelta
    }
  })();
}

export async function isItemVisibleInOutline(itemSelector) {
  const outlineState = await Selector('.c-outline')();
  const {
    top: outlineTop,
    bottom: outlineBottom
  } = outlineState.boundingClientRect;

  const itemState = await itemSelector();
  const {
    top: itemTop,
    bottom: itemBottom
  } = itemState.boundingClientRect;

  return itemBottom <= outlineBottom && itemTop >= outlineTop;
}

// Useful for sanity checking window size in the test env
// https://testcafe-discuss.devexpress.com/t/setting-the-browser-window-size-on-mac-os-x/170/6
export async function getSizeInfo() {
  return await ClientFunction(() => {
    return {
      width: window.innerWidth,
      height: window.innerHeight,
      outerWidth: window.outerWidth,
      outerHeight: window.outerHeight,
      availableWidth: screen.availWidth,
      availableHeight: screen.availHeight
    };
  });
}

/**
 * Url to writer interface.
 *
 * This assumes you've run `yarn start` and dev server is up
 *
 * @param options
 */
export function urlToWritingInterface({onboarding} = {}) {
  const params = [
    onboarding ? '' : 'skipOnboarding'
  ]
    .filter(param => !!param)
    .join('&');

  const queryString = params ? `?${params}` : '';
  return 'http://localhost:3000/' + queryString;
}

export const reloadPage = ClientFunction(() => location.reload());
