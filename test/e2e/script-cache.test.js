import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import {
  urlToWritingInterface,
  getEditorLines,
  reloadPage,
  preloadBitchPlanetScript,
} from './helpers';

/**
 * Tests for #48
 */
fixture('script cache')
  .page(urlToWritingInterface())

test('restore existing script', async t => {
  await preloadBitchPlanetScript();
  await reloadPage();

  await t
    // Spot check a few lines from the script
    .expect(getEditorLines()).contains('Subtitle: Too Big To Fail')
    .expect(getEditorLines()).contains('Dear Robert,');
});

test('restore latest changes after a pause', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .typeText(selectors.editorInput(), 'blah')
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      'blah'
    ])
    // Wait for latest changes to be cached. This makes assumptions about the
    // caching debounce period (1 sec) and may fail if that ever changes.
    .wait(1050);

  await reloadPage();

  await t
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      'blah'
    ]);
});

test('restore outline', async t => {
  await preloadBitchPlanetScript();
  await reloadPage();

  await t
    // Spot check a few items from the outline
    .expect(selectors.outlineItem('Adult Penny Rolle').count).eql(1)
    .expect(selectors.outlineItem('Glasses on now').count).eql(1)
});
