import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import * as helpers from './helpers';

fixture('insights')
  .page(helpers.urlToWritingInterface());

test('toggled by keyboard shortcut', async t => {
  await t
    .pressKey(shortcuts.insights)
    .expect(selectors.insightsPanel().exists).ok();

  await t
    .pressKey(shortcuts.insights)
    .expect(selectors.insightsPanel().exists).notOk();
});

test('shows panels per page graph', async t => {
  await helpers.preloadBitchPlanetScript();

  await t
    .pressKey(shortcuts.insights)
    .expect(selectors.insightsGraph('Panels per page').exists).ok();
});

test('shows spoken word count graph', async t => {
  await helpers.preloadBitchPlanetScript();

  await t
    .pressKey(shortcuts.insights)
    .expect(selectors.insightsGraph('Spoken words per page').exists).ok();
});
