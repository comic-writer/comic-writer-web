import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { getEditorLines, urlToWritingInterface, lettering } from './helpers';

fixture('editor toolbar')
  .page(urlToWritingInterface());

test('editor toolbar exists', async t => {
  await t.expect(selectors.editorToolbar().exists).ok();
});

test('inserting page on blank line', async t => {
  await t
    .click(selectors.editorToolbarButton('Page'))
    .expect(getEditorLines()).eql([
      'Page 1',
      ''
    ])
});

test('inserting page in middle of a line splits the line', async t => {
  await t
    .typeText(selectors.editorInput(), 'blah')
    .pressKey('left')
    .pressKey('left')
    .click(selectors.editorToolbarButton('Page'))
    .expect(getEditorLines()).eql([
      'bl',
      'Page 1',
      'ah',
    ]);
});

test('inserting page can be undone', async t => {
  await t
    .typeText(selectors.editorInput(), 'blah')
    .pressKey('left')
    .pressKey('left')
    .click(selectors.editorToolbarButton('Page'))
    .expect(getEditorLines()).eql([
      'bl',
      'Page 1',
      'ah',
    ])
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      'blah',
    ])
});

test('inserting panel on blank line', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .click(selectors.editorToolbarButton('Panel'))
    .expect(getEditorLines()).eql([
      'Page 1',
      'Panel 1',
      ''
    ]);
});

test('inserting panel in middle of a line splits the line', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .typeText(selectors.editorInput(), 'blah')
    .pressKey('left')
    .pressKey('left')
    .click(selectors.editorToolbarButton('Panel'))
    .expect(getEditorLines()).eql([
      'Page 1',
      'bl',
      'Panel 1',
      'ah',
    ])
});

test('inserting panel can be undone', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .typeText(selectors.editorInput(), 'blah')
    .pressKey('left')
    .pressKey('left')
    .click(selectors.editorToolbarButton('Panel'))
    .expect(getEditorLines()).eql([
      'Page 1',
      'bl',
      'Panel 1',
      'ah',
    ])
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      'Page 1',
      'blah',
    ])
});

test('inserting caption on blank line', async t => {
  await t
    .click(selectors.editorToolbarButton('Caption'))
    .expect(getEditorLines()).eql([
      lettering('CAPTION (): content')
    ])
});

test('caption can be undone', async t => {
  await t
    .click(selectors.editorToolbarButton('Caption'))
    .click(selectors.editorInput())
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      ''
    ])
});

test('inserting dialogue on blank line', async t => {
  await t
    .click(selectors.editorToolbarButton('Balloon'))
    .expect(getEditorLines()).eql([
      lettering('CHARACTER (): content')
    ])
});

test('dialogue can be undone', async t => {
  await t
    .click(selectors.editorToolbarButton('Balloon'))
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      ''
    ])
});

test('inserting sfx on blank line', async t => {
  await t
    .click(selectors.editorToolbarButton('SFX'))
    .expect(getEditorLines()).eql([
      lettering('SFX (): content')
    ])
});

test('sfx can be undone', async t => {
  await t
    .click(selectors.editorToolbarButton('SFX'))
    .click(selectors.editorInput())
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      ''
    ])
});

test('bold on blank line', async t => {
  await t
    .click(selectors.editorToolbarButton('Bold'))
    .expect(getEditorLines()).eql([
      '****'
    ])
});

test('bold can be undone', async t => {
  await t
    .click(selectors.editorToolbarButton('Bold'))
    .pressKey(shortcuts.undo)
    .expect(getEditorLines()).eql([
      ''
    ])
});
