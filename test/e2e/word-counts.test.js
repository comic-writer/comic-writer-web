import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { urlToWritingInterface } from './helpers';

fixture('word counts')
  .page(urlToWritingInterface());

test('captions have word counts', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'caption')
    .expect(selectors.wordCount(2).textContent).eql('1')
    .typeText(selectors.editorInput(), ' content')
    .expect(selectors.wordCount(2).textContent).eql('2')
});

test('dialogues have word counts', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'BOB')
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'dialogue')
    .expect(selectors.wordCount(2).textContent).eql('1')
    .typeText(selectors.editorInput(), ' content')
    .expect(selectors.wordCount(2).textContent).eql('2')
});

test('sfx do not have word counts', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.sfx)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'boom')
    .expect(selectors.wordCount(2).exists).notOk()
});

test('apostrophe words are 1 word', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'BOB')
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'it didn\'t')
    .expect(selectors.wordCount(2).textContent).eql('2')
});

test('hyphenated words are 2 words', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'BOB')
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'is it Spider-Man?')
    .expect(selectors.wordCount(2).textContent).eql('4')
});

test('page has no word count if it has no spoken words', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .expect(selectors.wordCount(0).exists).notOk();
});

test('panel has no word count if it has no spoken words', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .expect(selectors.wordCount(1).exists).notOk();
});

test('panel word count is sum of its lettering spoken word counts', async t => {
  const panelCount = selectors.wordCount(1);

  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // caption
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'caption content') // 2 spoken words
    .pressKey('enter')
    // balloon
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'BOB')
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'this is dialogue') // 3 spoken words
    .pressKey('enter')
    // sfx
    .pressKey(shortcuts.sfx)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'boom') // 0 spoken words
    .expect(panelCount.textContent).eql('5')
});

test('page word count is sum of its own lettering and its panel word counts', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    // caption directly in page
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'one two three') // 3 spoken words
    .pressKey('enter')
    // balloon directly in page
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'BOB')
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'blah') // 1 spoken word
    .pressKey('enter')
    // finally a panel
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.sfx)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'ka pow') // 0 spoken words
    .pressKey('enter')
    // caption
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'caption content') // 2 spoken words
    .pressKey('enter')
    // balloon
    .pressKey(shortcuts.dialogue)
    .typeText(selectors.editorInput(), 'BOB')
    .pressKey('tab')
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'this is dialogue') // 3 spoken words
    .pressKey('enter')
    // sfx
    .pressKey(shortcuts.sfx)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'boom') // 0 spoken words
    .pressKey('enter')
    // another panel
    .pressKey(shortcuts.panel)
    // caption
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'another caption') // 2 spoken words
    .expect(selectors.wordCount(0).textContent).eql('11')
});

test('other panels do not affect a panel\'s word count', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // caption
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'first panel') // 2 spoken words
    .pressKey('enter')
    // another panel
    .pressKey(shortcuts.panel)
    // caption
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'and another panel') // 3 spoken words
    .expect(selectors.wordCount(1).textContent).eql('2');
});

test('other pages do not affect a page\'s word count', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // caption
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'first page') // 2 spoken words
    .pressKey('enter')
    // another page
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // caption
    .pressKey(shortcuts.caption)
    .pressKey('tab')
    .typeText(selectors.editorInput(), 'second page') // 2 spoken words
    .expect(selectors.wordCount(0).textContent).eql('2')
});
