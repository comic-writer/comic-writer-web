import * as selectors from './selectors';
import * as shortcuts from './keyboard-shortcuts';
import { urlToWritingInterface } from './helpers';

fixture('panel counts')
  .page(urlToWritingInterface());

test('page with no panels yet', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .expect(selectors.panelCount(0).exists).notOk()
});

test('page with one panel', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .expect(selectors.panelCount(0).textContent).eql('1 panel')
});

test('page with two panels', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.panel)
    .expect(selectors.panelCount(0).textContent).eql('2 panels')
});

test('remove one panel from page with two panels', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.panel)
    // delete enough characters to make it no longer a panel
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(selectors.panelCount(0).textContent).eql('1 panel')
});

test('remove the only panel from page', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // delete enough characters to make it no longer a panel
    .pressKey('backspace')
    .pressKey('backspace')
    .expect(selectors.panelCount(0).exists).notOk();
});

test('panel counts are per page', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    .pressKey(shortcuts.panel)
    .expect(selectors.panelCount(0).textContent).eql('2 panels')
    .pressKey(shortcuts.page)
    // adding a panel to page 2 doesn't affect page 1's panel count
    .pressKey(shortcuts.panel)
    .expect(selectors.panelCount(0).textContent).eql('2 panels')
});

test('panel count goes away when its page line is no longer a page', async t => {
  await t
    .click(selectors.editorInput())
    .pressKey(shortcuts.page)
    .pressKey(shortcuts.panel)
    // move back to top line
    .pressKey('up')
    .pressKey('up')
    // move over to after last char
    .pressKey('right')
    .pressKey('right')
    .pressKey('right')
    .pressKey('right')
    .pressKey('right')
    .pressKey('right')
    // delete page number which makes that line no longer a page
    .pressKey('backspace')
    .expect(selectors.panelCount(0).exists).notOk();
});
