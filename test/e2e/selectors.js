import { Selector } from 'testcafe';

// Some commonly used testcafe Selectors
// https://devexpress.github.io/testcafe/documentation/test-api/selecting-page-elements/selectors/

/**
 * Collection of raw css selectors.
 */
export const css = {
  editorLine: '.CodeMirror-line',
  letteringContentToken: '.cm-lettering-content'
};

/**
 * Panel count by its position among all other panel counts.
 *
 * @param {number} index - Zero-based panel count number
 */
export function panelCount(index) {
  const options = {
    timeout: 100
  };

  return Selector(`.panel-count`, options).nth(index);
}

/**
 * The editor's content editable element.
 *
 * You can type into this element.
 */
export function editorInput() {
  return Selector('[contenteditable="true"]');
}

/**
 * Select lines in the editor.
 *
 * This probably selects multiple elements.
 */
export function editorLines() {
  return Selector(css.editorLine);
}

/**
 * Select the lettering hints popup.
 */
export function letteringHintsPopup() {
  return Selector('.CodeMirror-hints');
}

/**
 * Select an individual hint item.
 *
 * @param {number} index Zero-based index
 */
export function letteringHintsItem(index) {
  return allLetteringHintItems()
    .nth(index);
}

export function allLetteringHintItems() {
  return letteringHintsPopup()
    .child('.CodeMirror-hint');
}

/**
 * Select the line with the active lettering snippet.
 */
export function activeLetteringSnippet() {
  return Selector('.active-lettering-snippet');
}

/**
 * Word count by its position among all other word counts.
 *
 * @param {number} index - Zero-based word count number
 */
export function wordCount(index) {
  const options = {
    timeout: 100
  };

  return Selector('.word-count', options).nth(index);
}

export function outlineItem(textOrIndex) {
  if (typeof textOrIndex === 'string') {
    return allOutlineItems()
      .withText(textOrIndex)
      .nth(0);
  } else if (typeof textOrIndex === 'number') {
    return allOutlineItems()
      .nth(textOrIndex);
  } else {
    throw new Error(`Expected string or number but received: ${textOrIndex}`);
  }
}

export function currentOutlineItem() {
  return allOutlineItems().filter('.c-outline-item--current');
}

export function allOutlineItems() {
  return Selector('.c-outline-item');
}

export function pageLine(text) {
  return Selector('.cm-page').withExactText(text);
}

export function paragraphLine(text) {
  return Selector('.cm-paragraph').withExactText(text);
}

export function editorToolbar() {
  return Selector('.c-editor-tool-bar');
}

export function editorToolbarButton(label) {
  return editorToolbar()
    .find('button')
    .withExactText(label);
}

export function insightsPanel() {
  return Selector('.c-insights');
}

export function insightsGraph(text) {
  return insightsPanel().find('h2').withText(text);
}
