/*
 * Keyboard shortcuts for sending to Testcafe's `t.pressKey()`
 */

export const page = 'alt+1';
export const panel = 'alt+2';
export const dialogue = 'alt+3';
export const caption = 'alt+4';
export const sfx = 'alt+5';
export const bold = 'meta+b';

export const undo = 'meta+z';
export const redo = 'shift+meta+z';

export const insights = 'shift+meta+i';
