# Perf tests

## How to

1. Use functions exported from `src/perf/index.ts` to mark a section of code that needs its execution time measured.
2. Make some code changes.
3. Run `yarn run test:perf` and wait for it to finish (takes a minute or two).
4. Look at html reports in `test/perf/reports` to see the measured run times.
5. Repeat steps 2-4 as needed.

The html reports contain all previous measurements to help determine if recent changes made things faster or slower.

## Caution

Do not commit anything in `test/perf/reports` to git. That directory is excluded via `.gitignore`.

The report files are large and they're only relevant in the context of a single machine, so there's no need to share them.
